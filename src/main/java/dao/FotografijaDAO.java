package dao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import model.Komentar;
import model.Korisnik;
import model.Fotografija;

public class FotografijaDAO {
	
	private KorisnikDAO korisnikDAO;
	private Map<Integer, Fotografija> fotografije;
	private Map<Integer, Komentar> komentari;
	private static int idFotografije = 1;
	private static int idKomentar = 1;
	
	public FotografijaDAO(String putanja, KorisnikDAO korisnikDAO) {
		this.korisnikDAO = korisnikDAO;
		fotografije = new HashMap<Integer, Fotografija>();
		komentari = new HashMap<Integer, Komentar>();
		ucitajFotografije(putanja);
		ucitajKomentare(putanja);
	}

	private void ucitajFotografije(String putanja) {
		BufferedReader citac = null;
		try {
			File fajl = new File(putanja + "csv\\fotografije.csv");
			citac = new BufferedReader(new FileReader(fajl));
			String linija = "";
			while((linija = citac.readLine()) != null) {
				String[] parametri = linija.split(",");
				Fotografija f = new Fotografija(Integer.parseInt(parametri[0]), parametri[2], parametri[3], Boolean.parseBoolean(parametri[4]));
				Korisnik korisnik = korisnikDAO.dobaviKorisnika(parametri[1]);
				korisnik.dodajFotografiju(f);
				fotografije.put(Integer.parseInt(parametri[0]), f);
				idFotografije++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			if ( citac != null ) {
				try {
					citac.close();
				}
				catch (Exception e) { }
			}
		}
	}
	
	private void ucitajKomentare(String putanja) {
		BufferedReader citac = null;
		try {
			File fajl = new File(putanja + "csv\\komentari_fotografije.csv");
			citac = new BufferedReader(new FileReader(fajl));
			String linija = "";
			while((linija = citac.readLine()) != null) {
				String[] parametri = linija.split(",");
				Fotografija fotografija = fotografije.get(Integer.parseInt(parametri[1]));
				Date datumPostavljanja = izracunajDatum(parametri[4]);
				Date datumIzmene = null;
				if (!parametri[5].equals("null")) {
					datumIzmene = izracunajDatum(parametri[5]);
				}
				Komentar komentar = new Komentar(Integer.parseInt(parametri[0]), korisnikDAO.dobaviKorisnika(parametri[2]), parametri[3].replace("/ff", ","), 
									datumPostavljanja, datumIzmene, Boolean.parseBoolean(parametri[6]));
				fotografija.dodajKomentar(komentar);
				komentari.put(komentar.getId(), komentar);
				idKomentar++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			if ( citac != null ) {
				try {
					citac.close();
				}
				catch (Exception e) { }
			}
		}
		
	}
	
	private Date izracunajDatum(String datum) {
		try {
			return new SimpleDateFormat("DD.MM.YYYY.").parse(datum);
		} catch (ParseException e) {
			return null;
		}
	}

	public Fotografija dodajFotografiju(Fotografija fotografija, Korisnik ulogovani, String putanja) throws IOException {
		fotografija.setId(idFotografije);
		fotografija.setKomentari(new ArrayList<Komentar>());
		fotografija.setSlika("../slike/" + fotografija.getSlika());
		fotografije.put(idFotografije, fotografija);
		ulogovani.dodajFotografiju(fotografija);
		dodavanjeFotografijeFajl(putanja + "csv\\forografije.csv", fotografija, ulogovani);
		idFotografije++;
		return fotografija;
	}

	private void dodavanjeFotografijeFajl(String putanja, Fotografija fotografija, Korisnik ulogovani) throws IOException {
		Writer upis = new BufferedWriter(new FileWriter(putanja, true));
		upis.append(String.valueOf(fotografija.getId()));
		upis.append(",");
		upis.append(ulogovani.getKorisnickoIme());
		upis.append(",");
		upis.append(fotografija.getSlika());
		upis.append(",");
		upis.append(fotografija.getOpis().replace(",", "/ff"));
		upis.append(",");
		upis.append(String.valueOf(fotografija.isObrisan()));
		upis.append("\n");
		upis.flush();
		upis.close();
	}

	public Fotografija dobaviFotografiju(int id) {
		return fotografije.get(id);
	}

	public Fotografija obrisiFotografiju(Fotografija fotografija, String putanja) throws IOException {
		fotografija = fotografije.get(fotografija.getId());
		fotografija.setObrisan(true);
		for (Komentar komentar : fotografija.getKomentari()) {
			komentar.setObrisan(true);
		}
		ispisFajlaFotografije(putanja + "csv\\fotografije.csv");
		ispisFajlaKomentari(putanja + "csv\\komentari_fotografije.csv");
		return fotografija;
	}
	
	private void ispisFajlaFotografije(String putanja) throws IOException {
		Writer upis = new BufferedWriter(new FileWriter(putanja));
		for (Fotografija fotografija : fotografije.values()) {
			Korisnik korisnik = korisnikDAO.pronadjiKorisnikaPoSlici(fotografija);
			upis.append(String.valueOf(fotografija.getId()));
			upis.append(",");
			upis.append(korisnik.getKorisnickoIme());
			upis.append(",");
			upis.append(fotografija.getSlika());
			upis.append(",");
			upis.append(fotografija.getOpis().replace(",", "/ff"));
			upis.append(",");
			upis.append(String.valueOf(fotografija.isObrisan()));
			upis.append("\n");
		}
		upis.flush();
		upis.close();
	}
	
	private void ispisFajlaKomentari(String putanja) throws IOException {
		Writer upis = new BufferedWriter(new FileWriter(putanja));
		for (Fotografija fotografija : fotografije.values()) {
			for (Komentar komentar : fotografija.getKomentari()) {
				upis.append(String.valueOf(komentar.getId()));
				upis.append(",");
				upis.append(String.valueOf(fotografija.getId()));
				upis.append(",");
				upis.append(komentar.getKorisnik().getKorisnickoIme());
				upis.append(",");
				upis.append(komentar.getTekst().replace(",", "/ff"));
				upis.append(",");
				upis.append(datumString(komentar.getDatumKomentara()));
				upis.append(",");
				if (komentar.getDatumIzmene() == null) {
					upis.append("null");
				}else {
					upis.append(datumString(komentar.getDatumIzmene()));
				}
				upis.append(",");
				upis.append(String.valueOf(komentar.isObrisan()));
				upis.append("\n");
			}
		}
		upis.flush();
		upis.close();
	}
	
	@SuppressWarnings("deprecation")
	private String datumString(Date datum) {
		int dan = datum.getDate();
		int mesec = datum.getMonth() + 1;
		int godina = datum.getYear() + 1900;
		return dan + "." + mesec + "." + godina + ".";
	}

	public Komentar obrisiKomentar(Komentar komentar, String putanja) throws IOException {
		komentar = komentari.get(komentar.getId());
		komentar.setObrisan(true);
		ispisFajlaKomentari(putanja + "csv\\komentari_fotografije.csv");
		return komentar;
	}

	public Komentar dodajKomentar(Komentar komentar, String id, Korisnik ulogovani, String putanja) throws IOException {
		komentar.setId(idKomentar);
		komentar.setKorisnik(ulogovani);
		komentar.setDatumIzmene(null);
		komentari.put(idKomentar, komentar);
		Fotografija fotografija = fotografije.get(Integer.parseInt(id));
		fotografija.dodajKomentar(komentar);
		dodavanjeKomentarFajl(putanja + "csv\\komentari_fotografije.csv", komentar, id);
		idKomentar++;
		return komentar;
	}

	private void dodavanjeKomentarFajl(String putanja, Komentar komentar, String id) throws IOException {
		Writer upis = new BufferedWriter(new FileWriter(putanja, true));
		upis.append(String.valueOf(komentar.getId()));
		upis.append(",");
		upis.append(id);
		upis.append(",");
		upis.append(komentar.getKorisnik().getKorisnickoIme());
		upis.append(",");
		upis.append(komentar.getTekst().replace(",", "/ff"));
		upis.append(",");
		upis.append(datumString(komentar.getDatumKomentara()));
		upis.append(",");
		if (komentar.getDatumIzmene() == null) {
			upis.append("null");
		}else {
			upis.append(datumString(komentar.getDatumIzmene()));
		}
		upis.append(",");
		upis.append(String.valueOf(komentar.isObrisan()));
		upis.append("\n");
		upis.flush();
		upis.close();
	}
}
