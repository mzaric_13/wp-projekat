package dao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import model.Komentar;
import model.Korisnik;
import model.Objava;

public class ObjavaDAO {

	private KorisnikDAO korisnikDAO;
	private Map<Integer, Objava> objave;
	private Map<Integer, Komentar> komentari;
	private static int idObjava = 1;
	private static int idKomentar = 1;
	
	public ObjavaDAO() {
		objave = new HashMap<Integer, Objava>();
		komentari = new HashMap<Integer, Komentar>();
	}
	
	public ObjavaDAO(String putanja, KorisnikDAO korisnikDAO) {
		this.korisnikDAO = korisnikDAO;
		objave = new HashMap<Integer, Objava>();
		komentari = new HashMap<Integer, Komentar>();
		ucitajObjave(putanja);
		ucitajKomentare(putanja);
	}

	private void ucitajObjave(String putanja) {
		BufferedReader citac = null;
		try {
			File fajl = new File(putanja + "csv\\objave.csv");
			citac = new BufferedReader(new FileReader(fajl));
			String linija = "";
			while((linija = citac.readLine()) != null) {
				String[] parametri = linija.split(",");
				Objava objava = new Objava(Integer.parseInt(parametri[0]), parametri[2], parametri[3].replace("/ff", ","), Boolean.parseBoolean(parametri[4]));
				objave.put(Integer.parseInt(parametri[0]), objava);
				Korisnik korisnik = korisnikDAO.dobaviKorisnika(parametri[1]);
				korisnik.dodajObjavu(objava);
				idObjava++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			if ( citac != null ) {
				try {
					citac.close();
				}
				catch (Exception e) { }
			}
		}
	}
	
	private void ucitajKomentare(String putanja) {
		BufferedReader citac = null;
		try {
			File fajl = new File(putanja + "csv\\komentari.csv");
			citac = new BufferedReader(new FileReader(fajl));
			String linija = "";
			while((linija = citac.readLine()) != null) {
				String[] parametri = linija.split(",");
				Objava objava = objave.get(Integer.parseInt(parametri[1]));
				Date datumPostavljanja = izracunajDatum(parametri[4]);
				Date datumIzmene = null;
				if (!parametri[5].equals("null")) {
					datumIzmene = izracunajDatum(parametri[5]);
				}
				Komentar komentar = new Komentar(Integer.parseInt(parametri[0]), korisnikDAO.dobaviKorisnika(parametri[2]), parametri[3].replace("/ff", ","), 
						datumPostavljanja, datumIzmene, Boolean.parseBoolean(parametri[6]));
				objava.dodajKomentar(komentar);
				komentari.put(komentar.getId(), komentar);
				idKomentar++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			if ( citac != null ) {
				try {
					citac.close();
				}
				catch (Exception e) { }
			}
		}
		
	}
	
	private Date izracunajDatum(String datum) {
		try {
			return new SimpleDateFormat("DD.MM.YYYY.").parse(datum);
		} catch (ParseException e) {
			return null;
		}
	}

	public Objava dobaviObjavu(int id) {
		return objave.get(id);
	}

	public Objava obrisiObjavu(Objava objava, String putanja) throws IOException {
		objava = objave.get(objava.getId());
		objava.setObrisan(true);
		for (Komentar komentar : objava.getKomentari()) {
			komentar.setObrisan(true);
		}
		ispisFajlaObjave(putanja + "csv\\objave.csv");
		ispisFajlaKomentari(putanja + "csv\\komentari.csv");
		return objava;
	}
	
	private void ispisFajlaObjave(String putanja) throws IOException {
		Writer upis = new BufferedWriter(new FileWriter(putanja));
		for (Objava objava : objave.values()) {
			Korisnik korisnik = korisnikDAO.pronadjiKorisnikaPoObjavi(objava);
			upis.append(String.valueOf(objava.getId()));
			upis.append(",");
			upis.append(korisnik.getKorisnickoIme());
			upis.append(",");
			upis.append(objava.getSlika());
			upis.append(",");
			upis.append(objava.getTekst().replace(",", "/ff"));
			upis.append(",");
			upis.append(String.valueOf(objava.isObrisan()));
			upis.append("\n");
		}
		upis.flush();
		upis.close();
	}
	
	private void ispisFajlaKomentari(String putanja) throws IOException {
		Writer upis = new BufferedWriter(new FileWriter(putanja));
		for (Objava objava : objave.values()) {
			for (Komentar komentar : objava.getKomentari()) {
				upis.append(String.valueOf(komentar.getId()));
				upis.append(",");
				upis.append(String.valueOf(objava.getId()));
				upis.append(",");
				upis.append(komentar.getKorisnik().getKorisnickoIme());
				upis.append(",");
				upis.append(komentar.getTekst().replace(",", "/ff"));
				upis.append(",");
				upis.append(datumString(komentar.getDatumKomentara()));
				upis.append(",");
				if (komentar.getDatumIzmene() == null) {
					upis.append("null");
				}else {
					upis.append(datumString(komentar.getDatumIzmene()));
				}
				upis.append(",");
				upis.append(String.valueOf(komentar.isObrisan()));
				upis.append("\n");
			}
		}
		upis.flush();
		upis.close();
	}
	
	@SuppressWarnings("deprecation")
	private String datumString(Date datum) {
		int dan = datum.getDate();
		int mesec = datum.getMonth() + 1;
		int godina = datum.getYear() + 1900;
		return dan + "." + mesec + "." + godina + ".";
	}

	public Komentar obrisiKomentar(Komentar komentar, String putanja) throws IOException {
		komentar = komentari.get(komentar.getId());
		komentar.setObrisan(true);
		ispisFajlaKomentari(putanja + "csv\\komentari.csv");
		return komentar;
	}

	public Objava dodajObjavu(Objava objava, Korisnik ulogovani, String putanja) throws IOException {
		objava.setId(idObjava);
		objava.setKomentari(new ArrayList<Komentar>());
		if (objava.getSlika() != null) {
			objava.setSlika("../slike/" + objava.getSlika());
		}else {
			objava.setSlika("");
		}
		objave.put(idObjava, objava);
		ulogovani.dodajObjavu(objava);
		dodavanjeObjaveFajl(putanja + "csv\\objave.csv", objava, ulogovani);
		idObjava++;
		return objava;
	}
	
	private void dodavanjeObjaveFajl(String putanja, Objava objava, Korisnik ulogovani) throws IOException {
		Writer upis = new BufferedWriter(new FileWriter(putanja, true));
		upis.append(String.valueOf(objava.getId()));
		upis.append(",");
		upis.append(ulogovani.getKorisnickoIme());
		upis.append(",");
		upis.append(objava.getSlika());
		upis.append(",");
		upis.append(objava.getTekst().replace(",", "/ff"));
		upis.append(",");
		upis.append(String.valueOf(objava.isObrisan()));
		upis.append("\n");
		upis.flush();
		upis.close();
	}

	public Komentar dodajKomentar(Komentar komentar, String id, Korisnik ulogovani, String putanja) throws IOException {
		komentar.setId(idKomentar);
		komentar.setKorisnik(ulogovani);
		komentar.setDatumIzmene(null);
		komentari.put(idKomentar, komentar);
		Objava objava = objave.get(Integer.parseInt(id));
		objava.dodajKomentar(komentar);
		dodavanjeKomentarFajl(putanja + "csv\\komentari.csv", komentar, id);
		idKomentar++;
		return komentar;
	}

	private void dodavanjeKomentarFajl(String putanja, Komentar komentar, String id) throws IOException {
		Writer upis = new BufferedWriter(new FileWriter(putanja, true));
		upis.append(String.valueOf(komentar.getId()));
		upis.append(",");
		upis.append(id);
		upis.append(",");
		upis.append(komentar.getKorisnik().getKorisnickoIme());
		upis.append(",");
		upis.append(komentar.getTekst().replace(",", "/ff"));
		upis.append(",");
		upis.append(datumString(komentar.getDatumKomentara()));
		upis.append(",");
		if (komentar.getDatumIzmene() == null) {
			upis.append("null");
		}else {
			upis.append(datumString(komentar.getDatumIzmene()));
		}
		upis.append(",");
		upis.append(String.valueOf(komentar.isObrisan()));
		upis.append("\n");
		upis.flush();
		upis.close();
	}
}
