package dao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import model.Fotografija;
import model.Korisnik;
import model.Objava;
import model.Pol;
import model.Status;
import model.Uloga;
import model.ZahtevZaPrijateljstvo;

public class KorisnikDAO {
	
	private HashMap<String, Korisnik> korisnici;
	
	public KorisnikDAO() {
		korisnici = new HashMap<String, Korisnik>();
	}
	
	public KorisnikDAO(String putanja) {
		korisnici = new HashMap<String, Korisnik>();
		ucitajKorisnike(putanja);
		ucitajZahteveZaPrijateljstvo(putanja);
	}

	private void ucitajZahteveZaPrijateljstvo(String putanja) {
		BufferedReader citac = null;
		try {
			File fajl = new File(putanja + "csv\\zahteviZaPrijateljstvo.csv");
			citac = new BufferedReader(new FileReader(fajl));
			String linija = "";
			while ((linija = citac.readLine()) != null) {
				String[] parametri = linija.split(",");
				Korisnik posiljalac = dobaviKorisnika(parametri[0]);
				Korisnik primalac = dobaviKorisnika(parametri[1]);
				Status status = pretvoriUEnumeraciju(parametri[2]);
				Date datumSlanja = izracunajDatum(parametri[3]);
				boolean obrisan = Boolean.parseBoolean(parametri[4]);
				ZahtevZaPrijateljstvo z = new ZahtevZaPrijateljstvo(posiljalac, primalac, status, datumSlanja, obrisan);
				primalac.getZahteviZaPrijateljstvo().add(z);
				if (status == Status.PRIHVACENO && !obrisan) {
					posiljalac.getPrijatelji().add(primalac);
					primalac.getPrijatelji().add(posiljalac);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if ( citac != null ) {
				try {
					citac.close();
				}
				catch (Exception e) { }
			}
		}
	}

	private Status pretvoriUEnumeraciju(String status) {
		if (status.equals("PRIHVACENO")) {
			return Status.PRIHVACENO;
		}else if (status.equals("NA_CEKANJU")) {
			return Status.NA_CEKANJU;
		}else if (status.equals("ODBIJENO")) {
			return Status.ODBIJENO;
		}
		return null;
	}

	private void ucitajKorisnike(String putanja) {
		BufferedReader citac = null;
		try {
			File fajl = new File(putanja + "csv\\korisnici.csv");
			citac = new BufferedReader(new FileReader(fajl));
			String linija = "";
			while((linija = citac.readLine()) != null) {
				String[] parametri = linija.split(",");
				Pol pol = vratiPol(parametri[6]);
				Uloga uloga = vratiUlogu(parametri[7]);
				Date datumRodjenja = izracunajDatum(parametri[5]);
				Korisnik korisnik = new Korisnik(parametri[0], parametri[1], parametri[2], 
						parametri[3], parametri[4], datumRodjenja, pol, uloga, parametri[8], 
						Boolean.parseBoolean(parametri[9]), Boolean.parseBoolean(parametri[10]));
				korisnici.put(parametri[0], korisnik);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			if ( citac != null ) {
				try {
					citac.close();
				}
				catch (Exception e) { }
			}
		}
	}
	
	private Date izracunajDatum(String datum) {
		try {
			return new SimpleDateFormat("DD.MM.YYYY.").parse(datum);
		} catch (ParseException e) {
			return null;
		}
	}
	
	private Pol vratiPol(String pol) {
		if (pol.equals("MUSKO")) {
			return Pol.MUSKO;
		}else {
			return Pol.ZENSKO;
		}
	}
	
	private Uloga vratiUlogu(String uloga) {
		if (uloga.equals("ADMINISTRATOR")) {
			return Uloga.ADMINISTRATOR;
		}else {
			return Uloga.KORISNIK;
		}
	}
	
	public Collection<Korisnik> dobaviKorisnike() {
		return korisnici.values();
	}
	
	public Korisnik pronadjiKorisnikaPoObjavi(Objava objava) {
		for (Korisnik k : korisnici.values()) {
			for (Objava o : k.getObjave()) {
				if (o.getId() == objava.getId()) {
					return k;
				}
			}
		}
		return null;
	}
	
	public Korisnik pronadjiKorisnikaPoSlici(Fotografija slika) {
		for (Korisnik k : korisnici.values()) {
			for (Fotografija s : k.getFotografije()) {
				if (s.getId() == slika.getId()) {
					return k;
				}
			}
		}
		return null;
	}
	
	public Korisnik dodajKorisnika(Korisnik korisnik, String putanja) throws IOException {
		putanja += "csv\\korisnici.csv";
		if (korisnici.containsKey(korisnik.getKorisnickoIme())) {
			return null;
		}
		korisnik.setProfilnaSlika("../slike/profilna.jpg");
		korisnik.setUloga(Uloga.KORISNIK);
		korisnici.put(korisnik.getKorisnickoIme(), korisnik);
		upisKorisnikaUFajl(putanja, korisnik);
		return korisnik;
	}
	
	private void upisKorisnikaUFajl(String putanja, Korisnik korisnik) throws IOException {
		Writer upis = new BufferedWriter(new FileWriter(putanja, true));
		upis.append(korisnik.getKorisnickoIme());
		upis.append(",");
		upis.append(korisnik.getLozinka());
		upis.append(",");
		upis.append(korisnik.getEmail());
		upis.append(",");
		upis.append(korisnik.getIme());
		upis.append(",");
		upis.append(korisnik.getPrezime());
		upis.append(",");
		upis.append(datumString(korisnik.getDatumRodjenja()));
		upis.append(",");
		upis.append(korisnik.getPol().toString());
		upis.append(",");
		upis.append(korisnik.getUloga().toString());
		upis.append(",");
		upis.append(korisnik.getProfilnaSlika());
		upis.append(",");
		upis.append(String.valueOf(korisnik.isPrivatanNalog()));
		upis.append(",");
		upis.append(String.valueOf(korisnik.isAktivan()));
		upis.append("\n");
		upis.flush();
		upis.close();
	}
	
	@SuppressWarnings("deprecation")
	private String datumString(Date datum) {
		int dan = datum.getDate();
		int mesec = datum.getMonth() + 1;
		int godina = datum.getYear() + 1900;
		return dan + "." + mesec + "." + godina + ".";
	}

	public Korisnik dobaviKorisnika(String korisnickoIme) {
		if (korisnici.containsKey(korisnickoIme)) {
			return korisnici.get(korisnickoIme);
		}
		return null;
	}
	
	public Collection<Korisnik> pronadjiKorisnikePoKriterijumima(String ime, String prezime,
																String pocetniDatum, String krajnjiDatum) throws ParseException{
		
		if (pocetniDatum.length() < 10) {
			pocetniDatum = "1900-01-01";
		}
		if (krajnjiDatum.length() < 10) {
			krajnjiDatum = "2022-01-01";
		}
		Date pocetniDatumDate = new SimpleDateFormat("yyyy-MM-dd").parse(pocetniDatum);	
		Date krajnjiDatumDate = new SimpleDateFormat("yyyy-MM-dd").parse(krajnjiDatum);

		if (ime.length() < 1 && prezime.length() < 1) {
			return korisnici.values().stream().filter(i -> pocetniDatumDate.compareTo(i.getDatumRodjenja()) <= 0
					&& krajnjiDatumDate.compareTo(i.getDatumRodjenja()) >= 0
					&& i.getUloga() == Uloga.KORISNIK && i.isAktivan()).collect(Collectors.toList());
		}else if (ime.length() < 1) {
			return korisnici.values().stream().filter(i -> i.getPrezime().equals(prezime)
					&& pocetniDatumDate.compareTo(i.getDatumRodjenja()) <= 0
					&& krajnjiDatumDate.compareTo(i.getDatumRodjenja()) >= 0
					&& i.getUloga() == Uloga.KORISNIK && i.isAktivan()).collect(Collectors.toList());
		}else if (prezime.length() < 1) {
			return korisnici.values().stream().filter(i -> i.getIme().equals(ime)
					&& pocetniDatumDate.compareTo(i.getDatumRodjenja()) <= 0
					&& krajnjiDatumDate.compareTo(i.getDatumRodjenja()) >= 0
					&& i.getUloga() == Uloga.KORISNIK && i.isAktivan()).collect(Collectors.toList());
		}else {
			return korisnici.values().stream().filter(i -> i.getIme().equals(ime)
					&& i.getPrezime().equals(prezime)
					&& pocetniDatumDate.compareTo(i.getDatumRodjenja()) <= 0
					&& krajnjiDatumDate.compareTo(i.getDatumRodjenja()) >= 0
					&& i.getUloga() == Uloga.KORISNIK && i.isAktivan()).collect(Collectors.toList());
		}
	}
	
	public Collection<Korisnik> pronadjiKorisnikePoKriterijumimaUlogovani(Korisnik ulogovani, String ime,
			String prezime, String pocetniDatum, String krajnjiDatum) throws ParseException {
		if (pocetniDatum.length() < 10) {
			pocetniDatum = "1900-01-01";
		}
		if (krajnjiDatum.length() < 10) {
			krajnjiDatum = "2022-01-01";
		}
		Date pocetniDatumDate = new SimpleDateFormat("yyyy-MM-dd").parse(pocetniDatum);	
		Date krajnjiDatumDate = new SimpleDateFormat("yyyy-MM-dd").parse(krajnjiDatum);

		if (ime.length() < 1 && prezime.length() < 1) {
			return korisnici.values().stream().filter(i -> pocetniDatumDate.compareTo(i.getDatumRodjenja()) <= 0
					&& krajnjiDatumDate.compareTo(i.getDatumRodjenja()) >= 0 && (!i.equals(ulogovani))
					&& i.getUloga() == Uloga.KORISNIK && i.isAktivan()).collect(Collectors.toList());
		}else if (ime.length() < 1) {
			return korisnici.values().stream().filter(i -> i.getPrezime().equals(prezime)
					&& pocetniDatumDate.compareTo(i.getDatumRodjenja()) <= 0
					&& krajnjiDatumDate.compareTo(i.getDatumRodjenja()) >= 0 && (!i.equals(ulogovani))
					&& i.getUloga() == Uloga.KORISNIK && i.isAktivan()).collect(Collectors.toList());
		}else if (prezime.length() < 1) {
			return korisnici.values().stream().filter(i -> i.getIme().equals(ime)
					&& pocetniDatumDate.compareTo(i.getDatumRodjenja()) <= 0
					&& krajnjiDatumDate.compareTo(i.getDatumRodjenja()) >= 0 && (!i.equals(ulogovani))
					&& i.getUloga() == Uloga.KORISNIK && i.isAktivan() ).collect(Collectors.toList());
		}else {
			return korisnici.values().stream().filter(i -> i.getIme().equals(ime)
					&& i.getPrezime().equals(prezime)
					&& pocetniDatumDate.compareTo(i.getDatumRodjenja()) <= 0
					&& krajnjiDatumDate.compareTo(i.getDatumRodjenja()) >= 0 && (!i.equals(ulogovani))
					&& i.getUloga() == Uloga.KORISNIK && i.isAktivan()).collect(Collectors.toList());
		}
	}
	
	public Collection<Korisnik> pronadjiKorisnikeAdmin(String ime, String prezime, String email) {
		if (ime.length() < 1 && prezime.length() < 1 && email.length() > 0) {
			return korisnici.values().stream().filter(i -> email.equals(i.getEmail()) 
					&& i.getUloga() == Uloga.KORISNIK).collect(Collectors.toList());
		}else if (ime.length() < 1 && email.length() < 1 && prezime.length() > 0) {
			return korisnici.values().stream().filter(i -> prezime.equals(i.getPrezime()) 
					&& i.getUloga() == Uloga.KORISNIK).collect(Collectors.toList());
		}else if (prezime.length() < 1 && email.length() < 1 && ime.length() > 0) {
			return korisnici.values().stream().filter(i -> ime.equals(i.getIme()) 
					&& i.getUloga() == Uloga.KORISNIK).collect(Collectors.toList());
		}else if (ime.length() < 1 && prezime.length() > 0 && email.length() > 0) {
			return korisnici.values().stream().filter(i -> prezime.equals(i.getPrezime()) 
					&& email.equals(i.getEmail())
					&& i.getUloga() == Uloga.KORISNIK).collect(Collectors.toList());
		}else if (prezime.length() < 1 && ime.length() > 0 && email.length() > 0) {
			return korisnici.values().stream().filter(i -> ime.equals(i.getIme()) 
					&& email.equals(i.getEmail())
					&& i.getUloga() == Uloga.KORISNIK).collect(Collectors.toList());
		}else if (email.length() < 1 && ime.length() > 0 && prezime.length() > 0) {
			return korisnici.values().stream().filter(i -> ime.equals(i.getIme()) 
					&& prezime.equals(i.getPrezime())
					&& i.getUloga() == Uloga.KORISNIK).collect(Collectors.toList());
		}else if (ime.length() < 1 && prezime.length() < 1 && email.length() < 1){
			return korisnici.values().stream().filter(i -> i.getUloga() == Uloga.KORISNIK).collect(Collectors.toList());
		} else {
			return korisnici.values().stream().filter(i -> ime.equals(i.getIme()) 
					&& prezime.equals(i.getPrezime()) && email.equals(i.getEmail())
					&& i.getUloga() == Uloga.KORISNIK).collect(Collectors.toList());
		}
	}
	
	public Collection<Korisnik> sortirajKorisnike(String kriterijum, Collection<Korisnik> pretrazeniKorisnici){
		if (kriterijum.equals("ime")) {
			((List<Korisnik>) pretrazeniKorisnici).sort((Korisnik k1, Korisnik k2)->k1.getIme().compareTo(k2.getIme()));
		} else if (kriterijum.equals("prezime")) {
			((List<Korisnik>) pretrazeniKorisnici).sort((Korisnik k1, Korisnik k2)->k1.getPrezime().compareTo(k2.getPrezime()));
		}else if(kriterijum.equals("datum")) {
			((List<Korisnik>) pretrazeniKorisnici).sort((Korisnik k1, Korisnik k2)->k1.getDatumRodjenja().compareTo(k2.getDatumRodjenja()));
		}
		return (Collection<Korisnik>) pretrazeniKorisnici;
	}
	
	public List<Korisnik> getPrijatelje(Korisnik korisnik){
		List<Korisnik> aktuelniPrijatelji = new ArrayList<Korisnik>();
		for (Korisnik k : korisnik.getPrijatelji()) {
			if (k.isAktivan()) {
				aktuelniPrijatelji.add(k);
			}
		}
		return aktuelniPrijatelji;
	}

	public Korisnik promeniProfilnu(Korisnik korisnik, String slika, String putanja) throws IOException {
		korisnik.setProfilnaSlika(slika);
		putanja += "csv\\korisnici.csv";
		ispisCelogFajla(putanja);
		return korisnik;
	}
	
	
	public Collection<ZahtevZaPrijateljstvo> dobaviTrenutneZahteve(Korisnik k) {
		ArrayList<ZahtevZaPrijateljstvo> zahteviNaCekanju = new ArrayList<ZahtevZaPrijateljstvo>();
		for (ZahtevZaPrijateljstvo z : k.getZahteviZaPrijateljstvo()) {
			if (z.getStatus() == Status.NA_CEKANJU) {
				zahteviNaCekanju.add(z);
			}
		}
		return zahteviNaCekanju;
	}
	
	
	public ArrayList<ZahtevZaPrijateljstvo> izmeniZahtev(Korisnik korisnik, String podaci, String putanja) throws IOException{
		putanja += "csv\\zahteviZaPrijateljstvo.csv";
		String vrstaIzmene = podaci.split("_")[0];
		String korisnickoIme = podaci.split("_")[1];
		ArrayList<ZahtevZaPrijateljstvo> zahteviNaCekanju = new ArrayList<ZahtevZaPrijateljstvo>();
		
		for (ZahtevZaPrijateljstvo z : korisnik.getZahteviZaPrijateljstvo()) {
			if (z.getStatus() == model.Status.NA_CEKANJU &&
					z.getPosiljalac().getKorisnickoIme().equals(korisnickoIme) && !z.isObrisan()) {
				if (vrstaIzmene.equals("prihvati")) {
					z.setStatus(model.Status.PRIHVACENO);
					korisnik.getPrijatelji().add(dobaviKorisnika(z.getPosiljalac().getKorisnickoIme()));
					z.getPosiljalac().getPrijatelji().add(korisnik);
				} else if (vrstaIzmene.equals("odbij")) {
					z.setStatus(model.Status.ODBIJENO);
					z.setObrisan(true);;
				}
				ispisSvihZahteva(putanja);
			}
			else if (z.getStatus() == model.Status.NA_CEKANJU && !z.isObrisan()) {
				zahteviNaCekanju.add(z);
			}
		}
		return zahteviNaCekanju;
	}
	
	public Collection<Korisnik> dobaviZajednickePrijatelje(Korisnik ulogovani, String korisnickoIme){
		Collection<Korisnik> zajednickiPrijatelji = new ArrayList<Korisnik>();
		Korisnik drugi = dobaviKorisnika(korisnickoIme);
		if (ulogovani.getPrijatelji().size() == 0 || drugi.getPrijatelji().size() == 0) {
			return zajednickiPrijatelji;
		}
		for (Korisnik p1 : ulogovani.getPrijatelji()) {
			for (Korisnik p2 : drugi.getPrijatelji()) {
				if (p1.getKorisnickoIme().equals(p2.getKorisnickoIme()) && p1.isAktivan()) {
					zajednickiPrijatelji.add(p1);
				}
			}
		}
		return zajednickiPrijatelji;
	}
	
	public String proveraPrijateljstva(Korisnik ulogovani, String korisnickoIme) {
		Korisnik drugi = dobaviKorisnika(korisnickoIme);
		for (Korisnik k : ulogovani.getPrijatelji()) {
			if (k.getKorisnickoIme().equals(drugi.getKorisnickoIme()) && drugi.isAktivan()) {
				return "da"; 
			}
		}
		for (ZahtevZaPrijateljstvo z : ulogovani.getZahteviZaPrijateljstvo()) {
			if (z.getPosiljalac().equals(drugi)) {
				if (z.getStatus() == Status.NA_CEKANJU && !z.isObrisan()){
					return "mozda";
				}
				//cak iako ima vise odbijenih statusa, ako nisu prijatelji,
				//svi ce biti u stanju "odbijeno" (ili na_cekanju, ali se to prvo proverava)
				else if (z.getStatus() == Status.ODBIJENO && !z.isObrisan()) {
					return "ne";
				}
			}
		}
		for (ZahtevZaPrijateljstvo z : drugi.getZahteviZaPrijateljstvo()) {
			if (z.getPosiljalac().equals(ulogovani)) {
				if (z.getStatus() == Status.NA_CEKANJU && !z.isObrisan()){
					return "mozda";
				}
				//cak iako ima vise odbijenih statusa, ako nisu prijatelji,
				//svi ce biti u stanju "odbijeno" (ili na_cekanju, ali se to prvo proverava)
				else if (z.getStatus() == Status.ODBIJENO && !z.isObrisan()) {
					return "ne";
				}
			}
		}
		
		return "ne";
	}
	
	public Korisnik izmenaZahtevaZaPrijateljstvo(Korisnik ulogovani, String korisnickoIme, 
			String opcija, String putanja) throws IOException {
		boolean found = false;
		putanja += "csv\\zahteviZaPrijateljstvo.csv";
		Korisnik drugi = dobaviKorisnika(korisnickoIme);
		if (opcija.equals("izbrisi")) {
			ulogovani.getPrijatelji().remove(drugi);
			drugi.getPrijatelji().remove(ulogovani);
			for (ZahtevZaPrijateljstvo z: ulogovani.getZahteviZaPrijateljstvo()) {
				if (z.getPosiljalac().equals(drugi) && z.getStatus() == Status.PRIHVACENO && !z.isObrisan()) {
					z.setObrisan(true);
					found = true;
					break;
				}
			}
			if (!found) {
				for (ZahtevZaPrijateljstvo z1: drugi.getZahteviZaPrijateljstvo()) {
					if (z1.getPosiljalac().equals(ulogovani) && z1.getStatus() == Status.PRIHVACENO && !z1.isObrisan()) {
						z1.setObrisan(true);
						break;
					}
				}
			}
		}
		else if (opcija.equals("dodaj")) {
			Date date = new Date();
			ZahtevZaPrijateljstvo z = new ZahtevZaPrijateljstvo(ulogovani, drugi, Status.NA_CEKANJU, date, false);
			drugi.getZahteviZaPrijateljstvo().add(z);
		}
		ispisSvihZahteva(putanja);
		return drugi;
	}
	
	public Collection<Korisnik> promeniStatusKorisnika(String korisnickoIme, String putanja, Collection<Korisnik> dobavljeniKorisnici) 
			throws IOException{
		putanja += "csv\\korisnici.csv";
		Korisnik k = dobaviKorisnika(korisnickoIme);
		
		for (Korisnik kor : dobavljeniKorisnici) {
			if (kor.getKorisnickoIme().equals(korisnickoIme)) {
				if (kor.isAktivan()) {
					kor.setAktivan(false);
					k.setAktivan(false);
				}
				else if (!kor.isAktivan()) {
					kor.setAktivan(true);
					k.setAktivan(true);
				}
			}
		}
		
		ispisCelogFajla(putanja);
		return dobavljeniKorisnici;
	}


	private void ispisSvihZahteva(String putanja) throws IOException {
		Writer upis = new BufferedWriter(new FileWriter(putanja));
		for (Korisnik k : korisnici.values()) {
			for (ZahtevZaPrijateljstvo z : k.getZahteviZaPrijateljstvo()) {
				upis.append(z.getPosiljalac().getKorisnickoIme());
				upis.append(",");
				upis.append(z.getPrimalac().getKorisnickoIme());
				upis.append(",");
				upis.append(z.getStatus().name());
				upis.append(",");
				upis.append(datumString(z.getDatumSlanja()));
				upis.append(",");
				upis.append(String.valueOf(z.isObrisan()));
				upis.append("\n");
			}
		}
		upis.flush();
		upis.close();
	}

	public Korisnik izmenaPodataka(Korisnik korisnik, String putanja) throws IOException {
		Korisnik k = korisnici.get(korisnik.getKorisnickoIme());
		if (!korisnik.getEmail().equals(k.getEmail())) {
			k.setEmail(korisnik.getEmail());
		}
		if (!korisnik.getIme().equals(k.getIme())) {
			k.setIme(korisnik.getIme());
		}
		if (!korisnik.getEmail().equals(k.getEmail())) {
			k.setEmail(korisnik.getEmail());
		}
		if (!korisnik.getPrezime().equals(k.getPrezime())) {
			k.setPrezime(korisnik.getPrezime());
		}
		if (!korisnik.getPol().equals(k.getPol())) {
			k.setPol(korisnik.getPol());
		}
		if (!korisnik.getDatumRodjenja().equals(k.getDatumRodjenja())) {
			k.setDatumRodjenja(korisnik.getDatumRodjenja());
		}
		if (korisnik.isPrivatanNalog() != k.isPrivatanNalog()) {
			k.setPrivatanNalog(korisnik.isPrivatanNalog());
		}
		if (!korisnik.getLozinka().equals(k.getLozinka()) && !korisnik.getLozinka().equals("")) {
			k.setLozinka(korisnik.getLozinka());
		}
		if (!korisnik.getProfilnaSlika().equals(k.getProfilnaSlika()) && !korisnik.getProfilnaSlika().equals("")) {
			k.setProfilnaSlika(korisnik.getProfilnaSlika());
		}
		ispisCelogFajla(putanja + "csv\\korisnici.csv");
		return k;
	}
	
	private void ispisCelogFajla(String putanja) throws IOException {
		Writer upis = new BufferedWriter(new FileWriter(putanja));
		for (Korisnik korisnik : korisnici.values()) {
			upis.append(korisnik.getKorisnickoIme());
			upis.append(",");
			upis.append(korisnik.getLozinka());
			upis.append(",");
			upis.append(korisnik.getEmail());
			upis.append(",");
			upis.append(korisnik.getIme());
			upis.append(",");
			upis.append(korisnik.getPrezime());
			upis.append(",");
			upis.append(datumString(korisnik.getDatumRodjenja()));
			upis.append(",");
			upis.append(korisnik.getPol().toString());
			upis.append(",");
			upis.append(korisnik.getUloga().toString());
			upis.append(",");
			upis.append(korisnik.getProfilnaSlika());
			upis.append(",");
			upis.append(String.valueOf(korisnik.isPrivatanNalog()));
			upis.append(",");
			upis.append(String.valueOf(korisnik.isAktivan()));
			upis.append("\n");
		}
		upis.flush();
		upis.close();
	}

}

