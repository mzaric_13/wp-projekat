package dao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import model.Korisnik;
import model.Poruka;

public class PorukaDAO {
	
	private Collection<Poruka> poruke;
	
	public PorukaDAO(String putanja, KorisnikDAO korisnikDAO) {
		poruke = new ArrayList<Poruka>();
		ucitajPoruke(putanja, korisnikDAO);
	}

	private void ucitajPoruke(String putanja, KorisnikDAO korisnikDAO) {
		BufferedReader citac = null;
		try {
			File fajl = new File(putanja + "csv\\poruke.csv");
			citac = new BufferedReader(new FileReader(fajl));
			String linija = "";
			while((linija = citac.readLine()) != null) {
				String[] parametri = linija.split(",");
				Korisnik posiljalac = korisnikDAO.dobaviKorisnika(parametri[0]);
				Korisnik primalac = korisnikDAO.dobaviKorisnika(parametri[1]);
				Poruka poruka = new Poruka(posiljalac, primalac, parametri[2].replace("/ff", ","), izracunajDatum(parametri[3]));
				poruke.add(poruka);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			if ( citac != null ) {
				try {
					citac.close();
				}
				catch (Exception e) { }
			}
		}
	}
	
	private Date izracunajDatum(String datum) {
		try {
			return new SimpleDateFormat("DD.MM.YYYY.").parse(datum);
		} catch (ParseException e) {
			return null;
		}
	}
	
	public Poruka dodajPoruku(Poruka poruka, String putanja) throws IOException {
		poruke.add(poruka);
		dodavanjeporukeFajl(putanja + "csv\\poruke.csv", poruka);
		return poruka;
	} 
	
	private void dodavanjeporukeFajl(String putanja, Poruka poruka) throws IOException {
		Writer upis = new BufferedWriter(new FileWriter(putanja, true));
		upis.append(poruka.getPosiljalac().getKorisnickoIme());
		upis.append(",");
		upis.append(poruka.getPrimalac().getKorisnickoIme());
		upis.append(",");
		upis.append(poruka.getSadrzaj().replace(",", "/ff"));
		upis.append(",");
		upis.append(datumString(poruka.getDatumSlanja()));
		upis.append("\n");
		upis.flush();
		upis.close();
	}
	
	@SuppressWarnings("deprecation")
	private String datumString(Date datum) {
		int dan = datum.getDate();
		int mesec = datum.getMonth() + 1;
		int godina = datum.getYear() + 1900;
		return dan + "." + mesec + "." + godina + ".";
	}

	public Collection<Poruka> dobaviPoruke(String posiljalac, String primalac) {
		Collection<Poruka> konverzacija = new ArrayList<Poruka>();
		for (Poruka poruka : poruke) {
			if ((poruka.getPosiljalac().getKorisnickoIme().equals(posiljalac) && 
				poruka.getPrimalac().getKorisnickoIme().equals(primalac)) ||
				(poruka.getPosiljalac().getKorisnickoIme().equals(primalac) &&
				poruka.getPrimalac().getKorisnickoIme().equals(posiljalac))) {
				konverzacija.add(poruka);
			}
		}
		return konverzacija;
	}

	public Collection<Poruka> dobaviPorukeKorisnik(String korisnickoIme) {
		Collection<Poruka> porukeKorisnika = new ArrayList<Poruka>();
		for (Poruka poruka : poruke) {
			if (poruka.getPosiljalac().getKorisnickoIme().equals(korisnickoIme) ||  
				poruka.getPrimalac().getKorisnickoIme().equals(korisnickoIme)) {
				porukeKorisnika.add(poruka);
			}
		}
		return porukeKorisnika;
	}

}
