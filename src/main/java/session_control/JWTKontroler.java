package session_control;

import java.security.Key;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import dao.KorisnikDAO;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import model.Korisnik;

public class JWTKontroler {
	
	static Key key = Keys.secretKeyFor(SignatureAlgorithm.HS256);

	public void kreiranjeJWT(Korisnik korisnik) {
		String jws = Jwts.builder().setSubject(korisnik.getKorisnickoIme()).setExpiration(new Date(new Date().getTime() + 30 * 6000*10L)).setIssuedAt(new Date()).signWith(key).compact();
		korisnik.setJwt(jws);
	}
	
	public Korisnik proveriJWT(HttpServletRequest zahtev, KorisnikDAO korisnikDAO) {
		String auth = zahtev.getHeader("Authorization");
		if ((auth != null) && (auth.contains("Bearer "))) {
			String jwt = auth.substring(auth.indexOf("Bearer ") + 7);
			try {
			    Jws<Claims> claims = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(jwt);
			    String korisnickoIme = claims.getBody().getSubject();
				Korisnik korisnik = korisnikDAO.dobaviKorisnika(korisnickoIme);
				return korisnik;
			} catch (Exception e) {
				return null;
			}
		}
		return null;
	}
}
