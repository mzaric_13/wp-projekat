package model;

import java.util.Date;

public class Poruka {

	private Korisnik posiljalac;
	private Korisnik primalac;
	private String sadrzaj;
	private Date datumSlanja;
	
	public Poruka() {
		
	}

	public Poruka(Korisnik posiljalac, Korisnik primalac, String sadrzaj, Date datumSlanja) {
		this.posiljalac = posiljalac;
		this.primalac = primalac;
		this.sadrzaj = sadrzaj;
		this.datumSlanja = datumSlanja;
	}

	public Korisnik getPosiljalac() {
		return posiljalac;
	}

	public void setPosiljalac(Korisnik posiljalac) {
		this.posiljalac = posiljalac;
	}

	public Korisnik getPrimalac() {
		return primalac;
	}

	public void setPrimalac(Korisnik primalac) {
		this.primalac = primalac;
	}

	public String getSadrzaj() {
		return sadrzaj;
	}

	public void setSadrzaj(String sadrzaj) {
		this.sadrzaj = sadrzaj;
	}

	public Date getDatumSlanja() {
		return datumSlanja;
	}

	public void setDatumSlanja(Date datumSlanja) {
		this.datumSlanja = datumSlanja;
	}
	
	
}
