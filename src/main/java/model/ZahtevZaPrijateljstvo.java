package model;

import java.util.Date;

public class ZahtevZaPrijateljstvo {
	
	private Korisnik posiljalac;
	private Korisnik primalac;
	private Status status;
	private Date datumSlanja;
	private boolean obrisan;
	
	public ZahtevZaPrijateljstvo() {
		
	}
	
	public ZahtevZaPrijateljstvo(Korisnik posiljalac, Korisnik primalac, Status status, Date datumSlanja, boolean obrisan) {
		this.posiljalac = posiljalac;
		this.primalac = primalac;
		this.status = status;
		this.datumSlanja = datumSlanja;
		this.obrisan = obrisan;
	}

	public Korisnik getPosiljalac() {
		return posiljalac;
	}

	public void setPosiljalac(Korisnik posiljalac) {
		this.posiljalac = posiljalac;
	}

	public Korisnik getPrimalac() {
		return primalac;
	}

	public void setPrimalac(Korisnik primalac) {
		this.primalac = primalac;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Date getDatumSlanja() {
		return datumSlanja;
	}

	public void setDatumSlanja(Date datumSlanja) {
		this.datumSlanja = datumSlanja;
	}

	public boolean isObrisan() {
		return obrisan;
	}

	public void setObrisan(boolean obrisan) {
		this.obrisan = obrisan;
	}
	
	

}
