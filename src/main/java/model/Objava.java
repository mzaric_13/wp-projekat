package model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Objava {

	private int id;
	private String slika;
	private String tekst;
	@JsonIgnore
	private List<Komentar> komentari;
	private boolean obrisan;
	
	public Objava() {
		this.komentari = new ArrayList<Komentar>();
	}
	
	public Objava(int id, String slika, String tekst, boolean obrisan) {
		this.id = id;
		this.slika = slika;
		this.tekst = tekst;
		this.obrisan = obrisan;
		this.komentari = new ArrayList<Komentar>();
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSlika() {
		return slika;
	}

	public void setSlika(String slika) {
		this.slika = slika;
	}


	public String getTekst() {
		return tekst;
	}

	public void setTekst(String tekst) {
		this.tekst = tekst;
	}

	public List<Komentar> getKomentari() {
		return komentari;
	}

	public void setKomentari(List<Komentar> komentari) {
		this.komentari = komentari;
	}
	
	public void dodajKomentar(Komentar komentar) {
		this.komentari.add(komentar);
	}

	public boolean isObrisan() {
		return obrisan;
	}

	public void setObrisan(boolean obrisan) {
		this.obrisan = obrisan;
	}
	
}
