package model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Korisnik {
	
	private String korisnickoIme;
	private String lozinka;
	private String email;
	private String ime;
	private String prezime;
	private Date datumRodjenja;
	private Pol pol; 
	private Uloga uloga; 
	private String profilnaSlika;
	@JsonIgnore
	private List<Objava> objave; 
	@JsonIgnore
	private List<Fotografija> fotografije;
	@JsonIgnore
	private List<ZahtevZaPrijateljstvo> zahteviZaPrijateljstvo;
	@JsonIgnore
	private List<Korisnik> prijatelji; 
	private boolean privatanNalog;
	private boolean aktivan = true;
	private String jwt;
	
	public Korisnik() {
		this.objave = new ArrayList<Objava>();
		this.fotografije = new ArrayList<Fotografija>();
		this.zahteviZaPrijateljstvo = new ArrayList<ZahtevZaPrijateljstvo>();
		this.prijatelji = new ArrayList<Korisnik>();
	}
	
	public Korisnik(String korisnickoIme, String lozinka, String email, String ime, String prezime,
			Date datumRodjenja, Pol pol, Uloga uloga, String profilnaSlika, 
			boolean privatanNalog, boolean aktivan) {
		super();
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
		this.email = email;
		this.ime = ime;
		this.prezime = prezime;
		this.datumRodjenja = datumRodjenja;
		this.pol = pol;
		this.uloga = uloga;
		this.profilnaSlika = profilnaSlika;
		this.privatanNalog = privatanNalog;
		this.aktivan = aktivan;
		this.objave = new ArrayList<Objava>();
		this.fotografije = new ArrayList<Fotografija>();
		this.zahteviZaPrijateljstvo = new ArrayList<ZahtevZaPrijateljstvo>();
		this.prijatelji = new ArrayList<Korisnik>();
	}

	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public Date getDatumRodjenja() {
		return datumRodjenja;
	}

	public void setDatumRodjenja(Date datumRodjenja) {
		this.datumRodjenja = datumRodjenja;
	}

	public Pol getPol() {
		return pol;
	}

	public void setPol(Pol pol) {
		this.pol = pol;
	}

	public Uloga getUloga() {
		return uloga;
	}

	public void setUloga(Uloga uloga) {
		this.uloga = uloga;
	}

	public String getProfilnaSlika() {
		return profilnaSlika;
	}

	public void setProfilnaSlika(String profilnaSlika) {
		this.profilnaSlika = profilnaSlika;
	}

	public List<Objava> getObjave() {
		return objave;
	}

	public void setObjave(List<Objava> objave) {
		this.objave = objave;
	}

	public List<Fotografija> getFotografije() {
		return fotografije;
	}

	public void setFotografije(List<Fotografija> fotografije) {
		this.fotografije = fotografije;
	}

	public List<ZahtevZaPrijateljstvo> getZahteviZaPrijateljstvo() {
		return zahteviZaPrijateljstvo;
	}

	public void setZahteviZaPrijateljstvo(List<ZahtevZaPrijateljstvo> zahteviZaPrijateljstvo) {
		this.zahteviZaPrijateljstvo = zahteviZaPrijateljstvo;
	}

	public List<Korisnik> getPrijatelji() {
		return prijatelji;
	}

	public void setPrijatelji(List<Korisnik> prijatelji) {
		this.prijatelji = prijatelji;
	}

	public boolean isPrivatanNalog() {
		return privatanNalog;
	}

	public void setPrivatanNalog(boolean privatanNalog) {
		this.privatanNalog = privatanNalog;
	}

	public boolean isAktivan() {
		return aktivan;
	}

	public void setAktivan(boolean aktivan) {
		this.aktivan = aktivan;
	}
	
	public String getJwt() {
		return this.jwt;
	}
	
	public void setJwt(String jwt) {
		this.jwt = jwt;
	}

	public void dodajObjavu(Objava objava) {
		this.objave.add(objava);
	}

	public void dodajFotografiju(Fotografija fot) {
		this.fotografije.add(fot);
	}
}
