package model;

import java.util.Date;

public class Komentar {

	private int id;
	private Korisnik korisnik;
	private String tekst;
	private Date datumKomentara;
	private Date datumIzmene = null;
	private boolean obrisan;
	
	public Komentar() {
		
	}

	public Komentar(int id, Korisnik korisnik, String tekst, Date datumPostavljanja, Date datumIzmene, boolean obrisan) {
		this.id = id;
		this.korisnik = korisnik;
		this.tekst = tekst;
		this.datumKomentara = datumPostavljanja;
		this.datumIzmene = datumIzmene;
		this.obrisan = obrisan;
	}

	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}

	public Korisnik getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	} 	
	
	public String getTekst() {
		return tekst;
	}

	public void setTekst(String tekst) {
		this.tekst = tekst;
	}

	public Date getDatumKomentara() {
		return datumKomentara;
	}

	public void setDatumKomentara(Date datumKomentara) {
		this.datumKomentara = datumKomentara;
	}

	public Date getDatumIzmene() {
		return datumIzmene;
	}

	public void setDatumIzmene(Date datumIzmene) {
		this.datumIzmene = datumIzmene;
	}

	public boolean isObrisan() {
		return obrisan;
	}

	public void setObrisan(boolean obrisan) {
		this.obrisan = obrisan;
	}
}

