package model;

import java.util.ArrayList;
import java.util.List;

public class Fotografija {
	
	private int id;
	private String slika;
	private String opis;
	private List<Komentar> komentari;
	private boolean obrisan;
	
	public Fotografija() {
		this.komentari = new ArrayList<Komentar>();
	}

	public Fotografija(int id, String slika, String opis, boolean obrisan) {
		this.id = id;
		this.slika = slika;
		this.opis = opis;
		this.obrisan = obrisan;
		this.komentari = new ArrayList<Komentar>();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSlika() {
		return slika;
	}

	public void setSlika(String slika) {
		this.slika = slika;
	}

	public List<Komentar> getKomentari() {
		return komentari;
	}

	public void setKomentari(List<Komentar> komentari) {
		this.komentari = komentari;
	}

	public boolean isObrisan() {
		return obrisan;
	}

	public void setObrisan(boolean obrisan) {
		this.obrisan = obrisan;
	}

	public void dodajKomentar(Komentar komentar) {
		this.komentari.add(komentar);
		
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}
	
	

}
