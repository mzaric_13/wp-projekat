package service;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dao.KorisnikDAO;
import model.Korisnik;
import session_control.JWTKontroler;

@Path("/osnovniPodaci")
public class DobavljanjePodatakaProfilnaServis {
	
	@Context
	ServletContext kontekst;
	
	@PostConstruct
	public void init() {
		if (kontekst.getAttribute("korisnikDAO") == null) {
	    	String putanja = kontekst.getRealPath("");
			kontekst.setAttribute("korisnikDAO", new KorisnikDAO(putanja));
		}
		if (kontekst.getAttribute("jwtKontroler") == null) {
			kontekst.setAttribute("jwtKontroler", new JWTKontroler());
		}
	}
	
	@GET
	@Path("/dobaviPrijatelje")
	@Produces(MediaType.APPLICATION_JSON)
	public Response dobaviPrijatelje(@Context HttpServletRequest request) {
		JWTKontroler jwtKontroler = (JWTKontroler) kontekst.getAttribute("jwtKontroler");
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		Korisnik ulogovani = jwtKontroler.proveriJWT(request, korisnikDAO);
		if (ulogovani == null) {
			return Response.status(401).entity("Sesija vam je istekla!").build();
		}
		try {
			List<Korisnik> prijatelji = korisnikDAO.getPrijatelje(ulogovani);
			return Response.ok(prijatelji).build();
		} catch (Exception e) {
			return Response.status(401).entity("Niste ulogovani!").build();
		}
	}
	
	@GET
	@Path("/dobaviKorisnika")
	@Produces(MediaType.APPLICATION_JSON)
	public Response dobaviKorisnika(@Context HttpServletRequest request) {
		JWTKontroler jwtKontroler = (JWTKontroler) kontekst.getAttribute("jwtKontroler");
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		Korisnik ulogovani = jwtKontroler.proveriJWT(request, korisnikDAO);
		if (ulogovani == null) {
			return Response.status(401).entity("Sesija vam je istekla!").build();
		} else {
			return Response.ok(ulogovani).build();
		}
	}
	
	@POST
	@Path("/promeniProfilnu")
	@Produces(MediaType.APPLICATION_JSON)
	public Response promeniProfilnu(@Context HttpServletRequest request, @QueryParam("slika") String slika) throws IOException {
		JWTKontroler jwtKontroler = (JWTKontroler) kontekst.getAttribute("jwtKontroler");
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		Korisnik ulogovani = jwtKontroler.proveriJWT(request, korisnikDAO);
		String putanja = kontekst.getRealPath("");
		slika = "../slike/" + slika;
		Korisnik k = korisnikDAO.promeniProfilnu(ulogovani, slika, putanja);
		if (k == null) {
			return Response.status(400).entity("Gre�ka pri menjanju profilne slike.").build();
		}else if (ulogovani == null) {
			return Response.status(401).entity("Sesija vam je istekla!").build();
		}
		return Response.ok(k).build();
	}
}
