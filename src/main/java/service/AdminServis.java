package service;

import java.io.IOException;
import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dao.KorisnikDAO;
import model.Korisnik;
import session_control.JWTKontroler;

@Path("/admin")
public class AdminServis {
	@Context
	ServletContext kontekst;
	
	@PostConstruct
	public void init() {
		if (kontekst.getAttribute("korisnikDAO") == null) {
	    	String putanja = kontekst.getRealPath("");
			kontekst.setAttribute("korisnikDAO", new KorisnikDAO(putanja));
		}
		if (kontekst.getAttribute("jwtKontroler") == null) {
			kontekst.setAttribute("jwtKontroler", new JWTKontroler());
		}
	}
	
	@POST
	@Path("/promeniStatus")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response izmeniStatusKorisnika(@Context HttpServletRequest request, @QueryParam("id") String id, 
			Collection<Korisnik> korisnici) throws IOException {
		JWTKontroler jwtKontroler = (JWTKontroler) kontekst.getAttribute("jwtKontroler");
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		Korisnik ulogovani = jwtKontroler.proveriJWT(request, korisnikDAO);
		String putanja = kontekst.getRealPath("");
		Collection<Korisnik> izmenjeniKorisnici = korisnikDAO.promeniStatusKorisnika(id, putanja, korisnici);
		if (izmenjeniKorisnici == null) {
			return Response.status(400).entity("Do�lo je do gre�ke pri promeni stanja aktivnosti korisnika!").build();
		}
		if (ulogovani == null) {
			return Response.status(401).entity("Istekla je sesija").build();
		}
		return Response.ok(izmenjeniKorisnici).build();
	}
}
