package service;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dao.KorisnikDAO;
import dao.ObjavaDAO;
import model.Komentar;
import model.Korisnik;
import model.Objava;
import session_control.JWTKontroler;

@Path("/sopstveneObjave")
public class SopstveneObjaveServis {
	
	@Context
	ServletContext kontekst;
	
	@PostConstruct
	public void init() {
		if (kontekst.getAttribute("korisnikDAO") == null) {
	    	String putanja = kontekst.getRealPath("");
			kontekst.setAttribute("korisnikDAO", new KorisnikDAO(putanja));
		}
		if (kontekst.getAttribute("objavaDAO") == null) {
	    	String putanja = kontekst.getRealPath("");
	    	KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
			kontekst.setAttribute("objavaDAO", new ObjavaDAO(putanja, korisnikDAO));
		}
		if (kontekst.getAttribute("jwtKontroler") == null) {
			kontekst.setAttribute("jwtKontroler", new JWTKontroler());
		}
	}
	
	@GET
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response dobaviObjave(@Context HttpServletRequest zahtev) {
		JWTKontroler jwtKontroler = (JWTKontroler) kontekst.getAttribute("jwtKontroler");
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		Korisnik ulogovani = jwtKontroler.proveriJWT(zahtev, korisnikDAO);
		if (ulogovani != null) {
			if (ulogovani.getObjave().size() != 0) {
				return Response.ok(ulogovani.getObjave()).build();
			}
			return Response.status(404).entity("Nemate dodatih objava!").build();
		}
		return Response.status(401).entity("Sesija vam je istekla").build();
	}
	
	@GET
	@Path("/objava")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response dobaviObjavu(@Context HttpServletRequest zahtev, @QueryParam("id") String id) {
		JWTKontroler jwtKontroler = (JWTKontroler) kontekst.getAttribute("jwtKontroler");
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		ObjavaDAO objavaDAO = (ObjavaDAO) kontekst.getAttribute("objavaDAO");
		Korisnik ulogovani = jwtKontroler.proveriJWT(zahtev, korisnikDAO);
		if (ulogovani != null) {
			Objava objava = objavaDAO.dobaviObjavu(Integer.parseInt(id));
			if (objava != null) {
				return Response.ok(objava).build();
			}
			return Response.status(404).entity("Trazite nepostojecu objavu!").build();
		}
		return Response.status(401).entity("Sesija vam je istekla").build();
	}
	
	@PUT
	@Path("/brisanje")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response obrisiObjavu(@Context HttpServletRequest zahtev, Objava objava) {
		JWTKontroler jwtKontroler = (JWTKontroler) kontekst.getAttribute("jwtKontroler");
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		ObjavaDAO objavaDAO = (ObjavaDAO) kontekst.getAttribute("objavaDAO");
		Korisnik ulogovani = jwtKontroler.proveriJWT(zahtev, korisnikDAO);
		if (ulogovani != null) {
			String putanja = kontekst.getRealPath("");
			Objava o;
			try {
				o = objavaDAO.obrisiObjavu(objava, putanja);
				if (o != null) {
					return Response.ok(o).build();
				}
				return Response.status(400).entity("Greska pri brisanju objave").build();
			} catch (IOException e) {
				return Response.status(400).entity("Greska pri brisanju objave").build();
			}
			
		}
		return Response.status(401).entity("Sesija vam je istekla").build();
	}
	
	@GET
	@Path("/komentari")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response dobaviKomentare(@Context HttpServletRequest zahtev, @QueryParam("id") String id) {
		JWTKontroler jwtKontroler = (JWTKontroler) kontekst.getAttribute("jwtKontroler");
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		Korisnik ulogovani = jwtKontroler.proveriJWT(zahtev, korisnikDAO);
		ObjavaDAO objavaDAO = (ObjavaDAO) kontekst.getAttribute("objavaDAO");
		Objava objava = objavaDAO.dobaviObjavu(Integer.parseInt(id));
		if (ulogovani != null) {
			if (objava != null) {
				return Response.ok(objava.getKomentari()).build();
			}
			return Response.status(404).entity("Objava ne postoji!").build();
		}
		return Response.status(401).entity("Sesija vam je istekla").build();
	}
	
	@PUT
	@Path("/brisanjeKomentar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response obrisiKomentar(@Context HttpServletRequest zahtev, Komentar komentar) {
		JWTKontroler jwtKontroler = (JWTKontroler) kontekst.getAttribute("jwtKontroler");
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		ObjavaDAO objavaDAO = (ObjavaDAO) kontekst.getAttribute("objavaDAO");
		Korisnik ulogovani = jwtKontroler.proveriJWT(zahtev, korisnikDAO);
		if (ulogovani != null) {
			String putanja = kontekst.getRealPath("");
			Komentar k;
			try {
				k = objavaDAO.obrisiKomentar(komentar, putanja);
				if (k != null) {
					return Response.ok(k).build();
				}
				return Response.status(400).entity("Greska pri brisanju komentara").build();
			} catch (IOException e) {
				return Response.status(400).entity("Greska pri brisanju komentara").build();
			}
			
		}
		return Response.status(401).entity("Sesija vam je istekla").build();
	}
	
	@POST
	@Path("/dodajObjavu")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response dodajObjavu(@Context HttpServletRequest zahtev, Objava objava) {
		JWTKontroler jwtKontroler = (JWTKontroler) kontekst.getAttribute("jwtKontroler");
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		ObjavaDAO objavaDAO = (ObjavaDAO) kontekst.getAttribute("objavaDAO");
		Korisnik ulogovani = jwtKontroler.proveriJWT(zahtev, korisnikDAO);
		if (ulogovani != null) {
			String putanja = kontekst.getRealPath("");
			try {
				objava = objavaDAO.dodajObjavu(objava, ulogovani, putanja);
				if (objava != null) {
					return Response.ok(objava).build();
				}
				return Response.status(400).entity("Greska pri dodavanju objave").build();
			} catch (IOException e) {
				return Response.status(400).entity("Greska pri dodavanju objave").build();
			}
			
		}
		return Response.status(401).entity("Sesija vam je istekla").build();
	}

}
