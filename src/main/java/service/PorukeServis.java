package service;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dao.KorisnikDAO;
import dao.PorukaDAO;
import model.Korisnik;
import model.Poruka;
import session_control.JWTKontroler;

@Path("/poruke")
public class PorukeServis {

	@Context
	ServletContext kontekst;
	
	@PostConstruct
	public void init() {
		if (kontekst.getAttribute("korisnikDAO") == null) {
	    	String putanja = kontekst.getRealPath("");
			kontekst.setAttribute("korisnikDAO", new KorisnikDAO(putanja));
		}
		if (kontekst.getAttribute("porukaDAO") == null) {
	    	String putanja = kontekst.getRealPath("");
	    	KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
			kontekst.setAttribute("porukaDAO", new PorukaDAO(putanja, korisnikDAO));
		}
		if (kontekst.getAttribute("jwtKontroler") == null) {
			kontekst.setAttribute("jwtKontroler", new JWTKontroler());
		}
	}
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response dobaviPoruke(@Context HttpServletRequest zahtev, @QueryParam("primalac") String primalac) {
		JWTKontroler jwtKontroler = (JWTKontroler) kontekst.getAttribute("jwtKontroler");
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		Korisnik ulogovani = jwtKontroler.proveriJWT(zahtev, korisnikDAO);
		PorukaDAO porukaDAO = (PorukaDAO) kontekst.getAttribute("porukaDAO");
		if (ulogovani != null) {
			return Response.ok(porukaDAO.dobaviPoruke(ulogovani.getKorisnickoIme(), primalac)).build();
		}
		return Response.status(401).entity("Sesija vam je istekla").build();
	}
	
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response dodajPoruku(@Context HttpServletRequest zahtev, Poruka poruka) {
		JWTKontroler jwtKontroler = (JWTKontroler) kontekst.getAttribute("jwtKontroler");
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		Korisnik ulogovani = jwtKontroler.proveriJWT(zahtev, korisnikDAO);
		PorukaDAO porukaDAO = (PorukaDAO) kontekst.getAttribute("porukaDAO");
		String putanja = kontekst.getRealPath("");
		if (ulogovani != null) {
			try {
				poruka = porukaDAO.dodajPoruku(poruka, putanja);
				return Response.ok(poruka).build();
			} catch (IOException e) {
				return Response.status(500).entity("Greska pri dodavanju poruke!").build();
			}
		}
		return Response.status(401).entity("Sesija vam je istekla").build();
	}
	
	@GET
	@Path("/korisnik")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response dobaviPorukeKorisnika(@Context HttpServletRequest zahtev) {
		JWTKontroler jwtKontroler = (JWTKontroler) kontekst.getAttribute("jwtKontroler");
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		Korisnik ulogovani = jwtKontroler.proveriJWT(zahtev, korisnikDAO);
		PorukaDAO porukaDAO = (PorukaDAO) kontekst.getAttribute("porukaDAO");
		if (ulogovani != null) {
			return Response.ok(porukaDAO.dobaviPorukeKorisnik(ulogovani.getKorisnickoIme())).build();
		}
		return Response.status(401).entity("Sesija vam je istekla").build();
	}
	
}
