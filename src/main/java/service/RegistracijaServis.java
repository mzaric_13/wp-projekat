package service;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dao.KorisnikDAO;
import model.Korisnik;

@Path("/registracija")
public class RegistracijaServis {

	@Context
	ServletContext kontekst;
	
	@PostConstruct
	public void init() {
		if (kontekst.getAttribute("korisnikDAO") == null) {
	    	String putanja = kontekst.getRealPath("");
			kontekst.setAttribute("korisnikDAO", new KorisnikDAO(putanja));
		}
	}
	
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response dodajKorisnika(Korisnik korisnik) {
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		String putanja = kontekst.getRealPath("");
		try {
			Korisnik k = korisnikDAO.dodajKorisnika(korisnik, putanja);
			if (k == null) {
				return Response.status(400).entity("Korisnik sa datim korisnickim imenom vec postoji!").build();
			}
			return Response.ok(k).build();
		} catch (Exception e) {
			return Response.status(500).entity("Greska pri registraciji!").build();
		}
	}
}
