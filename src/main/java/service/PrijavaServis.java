package service;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dao.KorisnikDAO;
import model.Korisnik;
import session_control.JWTKontroler;

@Path("/prijava")
public class PrijavaServis {

	@Context
	ServletContext kontekst;
	
	@PostConstruct
	public void init() {
		if (kontekst.getAttribute("korisnikDAO") == null) {
	    	String putanja = kontekst.getRealPath("");
			kontekst.setAttribute("korisnikDAO", new KorisnikDAO(putanja));
		}
		if (kontekst.getAttribute("jwtKontroler") == null) {
			kontekst.setAttribute("jwtKontroler", new JWTKontroler());
		}
	}
	
	@POST
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response prijava(@QueryParam("korisnickoIme") String korisnickoIme, @QueryParam("lozinka") String lozinka) {
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		JWTKontroler jwtKontroler = (JWTKontroler) kontekst.getAttribute("jwtKontroler");
		Korisnik korisnik = korisnikDAO.dobaviKorisnika(korisnickoIme);
		if (korisnik == null) {
			return Response.status(401).entity("Uneli ste pogresno korisnicko ime!").build();
		}
		if (!korisnik.isAktivan()) {
			return Response.status(401).entity("Uneli ste pogresno korisnicko ime!").build();
		}
		if (lozinka.equals(korisnik.getLozinka())) {
			jwtKontroler.kreiranjeJWT(korisnik);
			return Response.ok(korisnik).build();
		}
		return Response.status(401).entity("Uneli ste pogresnu lozinku!").build();
	}
	
	@GET
	@Path("/odjava")
	@Produces(MediaType.APPLICATION_JSON)
	public Response odjava(@Context HttpServletRequest zahtev) {
		JWTKontroler jwtKontroler = (JWTKontroler) kontekst.getAttribute("jwtKontroler");
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		Korisnik korisnik = jwtKontroler.proveriJWT(zahtev, korisnikDAO);
		if (korisnik != null) {
			korisnik.setJwt("");
			return Response.ok(korisnik).build();
		}
		return Response.status(400).entity("Niste ulogovani").build();
	}
	
	@GET
	@Path("/provera")
	@Produces(MediaType.APPLICATION_JSON)
	public Response provera(@Context HttpServletRequest zahtev) {
		JWTKontroler jwtKontroler = (JWTKontroler) kontekst.getAttribute("jwtKontroler");
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		Korisnik korisnik = jwtKontroler.proveriJWT(zahtev, korisnikDAO);
		if (korisnik != null) {
			return Response.ok(korisnik).build();
		}
		return Response.status(400).entity("Niste ulogovani").build();
	}
}

