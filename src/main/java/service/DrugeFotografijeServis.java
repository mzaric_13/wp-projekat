package service;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dao.FotografijaDAO;
import dao.KorisnikDAO;
import model.Fotografija;
import model.Komentar;
import model.Korisnik;
import session_control.JWTKontroler;

@Path("/drugeSlike")
public class DrugeFotografijeServis {

	@Context
	ServletContext kontekst;
	
	@PostConstruct
	public void init() {
		if (kontekst.getAttribute("korisnikDAO") == null) {
	    	String putanja = kontekst.getRealPath("");
			kontekst.setAttribute("korisnikDAO", new KorisnikDAO(putanja));
		}
		if (kontekst.getAttribute("fotografijaDAO") == null) {
	    	String putanja = kontekst.getRealPath("");
	    	KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
			kontekst.setAttribute("fotografijaDAO", new FotografijaDAO(putanja, korisnikDAO));
		}
		if (kontekst.getAttribute("jwtKontroler") == null) {
			kontekst.setAttribute("jwtKontroler", new JWTKontroler());
		}
	}
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response dobaviFotografije(@Context HttpServletRequest zahtev, @QueryParam("korisnickoIme") String korisnickoIme) {
		JWTKontroler jwtKontroler = (JWTKontroler) kontekst.getAttribute("jwtKontroler");
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		Korisnik ulogovani = jwtKontroler.proveriJWT(zahtev, korisnikDAO);
		Korisnik korisnik = korisnikDAO.dobaviKorisnika(korisnickoIme);
		if (ulogovani != null) {
			if (korisnik != null) {
				return Response.ok(korisnik.getFotografije()).build();
			}
			return Response.status(404).entity("Korisnik ne postoji!").build();
		}
		return Response.status(401).entity("Sesija vam je istekla").build();
	}
	
	@GET
	@Path("/bezJwt")
	@Produces(MediaType.APPLICATION_JSON)
	public Response dobaviFotografijeBezJwt(@Context HttpServletRequest zahtev, @QueryParam("korisnickoIme") String korisnickoIme) {
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		Korisnik korisnik = korisnikDAO.dobaviKorisnika(korisnickoIme);
		if (korisnik != null) {
			return Response.ok(korisnik.getFotografije()).build();
		}
		return Response.status(404).entity("Korisnik ne postoji!").build();
	}
	
	@GET
	@Path("/slika")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response dobaviFotografiju(@Context HttpServletRequest zahtev, @QueryParam("id") String id) {
		JWTKontroler jwtKontroler = (JWTKontroler) kontekst.getAttribute("jwtKontroler");
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		FotografijaDAO fotografijaDAO = (FotografijaDAO) kontekst.getAttribute("fotografijaDAO");
		Korisnik ulogovani = jwtKontroler.proveriJWT(zahtev, korisnikDAO);
		if (ulogovani != null) {
			Fotografija f = fotografijaDAO.dobaviFotografiju(Integer.parseInt(id));
			if (f != null) {
				return Response.ok(f).build();
			}
			return Response.status(404).entity("Trazite nepostojecu fotografiju!").build();
		}
		return Response.status(401).entity("Sesija vam je istekla").build();
	}
	
	@POST
	@Path("/komentari")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response dodajKomentar(@Context HttpServletRequest zahtev, Komentar komentar, @QueryParam("idSlike") String idSlike) {
		JWTKontroler jwtKontroler = (JWTKontroler) kontekst.getAttribute("jwtKontroler");
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		Korisnik ulogovani = jwtKontroler.proveriJWT(zahtev, korisnikDAO);
		FotografijaDAO fotografijaDAO = (FotografijaDAO) kontekst.getAttribute("fotografijaDAO");
		if (ulogovani != null) {
			String putanja = kontekst.getRealPath("");
			try {
				komentar = fotografijaDAO.dodajKomentar(komentar, idSlike, ulogovani, putanja);
				if (komentar != null) {
					return Response.ok(komentar).build();
				}
				return Response.status(400).entity("Greska pri dodavanju komentara").build();
			} catch (IOException e) {
				return Response.status(400).entity("Greska pri dodavanju komentara").build();
			}
			
		}
		return Response.status(401).entity("Sesija vam je istekla").build();
	}
	
	@PUT
	@Path("/brisanjeKomentar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response obrisiKomentar(@Context HttpServletRequest zahtev, Komentar komentar) {
		JWTKontroler jwtKontroler = (JWTKontroler) kontekst.getAttribute("jwtKontroler");
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		FotografijaDAO fotografijaDAO = (FotografijaDAO) kontekst.getAttribute("fotografijaDAO");
		Korisnik ulogovani = jwtKontroler.proveriJWT(zahtev, korisnikDAO);
		if (ulogovani != null) {
			String putanja = kontekst.getRealPath("");
			Komentar k;
			try {
				k = fotografijaDAO.obrisiKomentar(komentar, putanja);
				if (k != null) {
					return Response.ok(k).build();
				}
				return Response.status(400).entity("Greska pri brisanju komentara").build();
			} catch (IOException e) {
				return Response.status(400).entity("Greska pri brisanju komentara").build();
			}
			
		}
		return Response.status(401).entity("Sesija vam je istekla").build();
	}
	
	@POST
	@Path("/izbrisiSliku")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response obrisiSliku(@Context HttpServletRequest zahtev, @QueryParam("idSlike") String idSlike) {
		JWTKontroler jwtKontroler = (JWTKontroler) kontekst.getAttribute("jwtKontroler");
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		FotografijaDAO fotografijaDAO = (FotografijaDAO) kontekst.getAttribute("fotografijaDAO");
		Korisnik ulogovani = jwtKontroler.proveriJWT(zahtev, korisnikDAO);
		if (ulogovani != null) {
			String putanja = kontekst.getRealPath("");
			Fotografija slika = fotografijaDAO.dobaviFotografiju(Integer.parseInt(idSlike));
			Fotografija f;
			Korisnik k = korisnikDAO.pronadjiKorisnikaPoSlici(slika);
			if (k == null) {
				return Response.status(400).entity("Greska pri pronalazenju korisnika").build();
			}
			try {				
				f = fotografijaDAO.obrisiFotografiju(slika, putanja);
				if (f != null) {
					return Response.ok(k).build();
				}
				return Response.status(400).entity("Greska pri brisanju slike").build();
			} catch (IOException e) {
				return Response.status(400).entity("Greska pri brisanju objave").build();
			}
		}
		return Response.status(401).entity("Sesija vam je istekla").build();
	}
}
