package service;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dao.FotografijaDAO;
import dao.KorisnikDAO;
import model.Fotografija;
import model.Komentar;
import model.Korisnik;
import session_control.JWTKontroler;

@Path("/sopstveneSlike")
public class SopstveneFotografijeServis {
	
	@Context
	ServletContext kontekst;
	
	@PostConstruct
	public void init() {
		if (kontekst.getAttribute("korisnikDAO") == null) {
	    	String putanja = kontekst.getRealPath("");
			kontekst.setAttribute("korisnikDAO", new KorisnikDAO(putanja));
		}
		if (kontekst.getAttribute("fotografijaDAO") == null) {
	    	String putanja = kontekst.getRealPath("");
	    	KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
			kontekst.setAttribute("fotografijaDAO", new FotografijaDAO(putanja, korisnikDAO));
		}
		if (kontekst.getAttribute("jwtKontroler") == null) {
			kontekst.setAttribute("jwtKontroler", new JWTKontroler());
		}
	}
	
	@GET
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response dobaviFotografije(@Context HttpServletRequest zahtev) {
		JWTKontroler jwtKontroler = (JWTKontroler) kontekst.getAttribute("jwtKontroler");
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		Korisnik ulogovani = jwtKontroler.proveriJWT(zahtev, korisnikDAO);
		if (ulogovani != null) {
			if (ulogovani.getFotografije().size() != 0) {
				return Response.ok(ulogovani.getFotografije()).build();
			}
			return Response.status(404).entity("Nemate dodatih fotografija!").build();
		}
		return Response.status(401).entity("Sesija vam je istekla").build();
	}
	
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response dodajFotografiju(@Context HttpServletRequest zahtev, Fotografija fotografija) {
		JWTKontroler jwtKontroler = (JWTKontroler) kontekst.getAttribute("jwtKontroler");
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		FotografijaDAO fotografijaDAO = (FotografijaDAO) kontekst.getAttribute("fotografijaDAO");
		Korisnik ulogovani = jwtKontroler.proveriJWT(zahtev, korisnikDAO);
		if (ulogovani != null) {
			String putanja = kontekst.getRealPath("");
			try {
				fotografija = fotografijaDAO.dodajFotografiju(fotografija, ulogovani, putanja);
				if (fotografija != null) {
					return Response.ok(fotografija).build();
				}
				return Response.status(400).entity("Greska pri dodavanju fotografije").build();
			} catch (IOException e) {
				return Response.status(400).entity("Greska pri dodavanju fotografije").build();
			}
			
		}
		return Response.status(401).entity("Sesija vam je istekla").build();
	}

	@GET
	@Path("/slika")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response dobaviFotografiju(@Context HttpServletRequest zahtev, @QueryParam("id") String id) {
		JWTKontroler jwtKontroler = (JWTKontroler) kontekst.getAttribute("jwtKontroler");
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		FotografijaDAO fotografijaDAO = (FotografijaDAO) kontekst.getAttribute("fotografijaDAO");
		Korisnik ulogovani = jwtKontroler.proveriJWT(zahtev, korisnikDAO);
		if (ulogovani != null) {
			Fotografija fotografija = fotografijaDAO.dobaviFotografiju(Integer.parseInt(id));
			if (fotografija != null) {
				return Response.ok(fotografija).build();
			}
			return Response.status(404).entity("Trazite nepostojecu fotografiju!").build();
		}
		return Response.status(401).entity("Sesija vam je istekla").build();
	}
	
	@PUT
	@Path("/brisanje")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response obrisiFotografiju(@Context HttpServletRequest zahtev, Fotografija fotografija) {
		JWTKontroler jwtKontroler = (JWTKontroler) kontekst.getAttribute("jwtKontroler");
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		FotografijaDAO fotografijaDAO = (FotografijaDAO) kontekst.getAttribute("fotografijaDAO");
		Korisnik ulogovani = jwtKontroler.proveriJWT(zahtev, korisnikDAO);
		if (ulogovani != null) {
			String putanja = kontekst.getRealPath("");
			Fotografija f;
			try {
				f = fotografijaDAO.obrisiFotografiju(fotografija, putanja);
				if (f != null) {
					return Response.ok(f).build();
				}
				return Response.status(400).entity("Greska pri brisanju fotografije").build();
			} catch (IOException e) {
				return Response.status(400).entity("Greska pri brisanju fotografije").build();
			}
			
		}
		return Response.status(401).entity("Sesija vam je istekla").build();
	}

	@PUT
	@Path("/brisanjeKomentar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response obrisiKomentar(@Context HttpServletRequest zahtev, Komentar komentar) {
		JWTKontroler jwtKontroler = (JWTKontroler) kontekst.getAttribute("jwtKontroler");
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		FotografijaDAO fotografijaDAO = (FotografijaDAO) kontekst.getAttribute("fotografijaDAO");
		Korisnik ulogovani = jwtKontroler.proveriJWT(zahtev, korisnikDAO);
		if (ulogovani != null) {
			String putanja = kontekst.getRealPath("");
			Komentar k;
			try {
				k = fotografijaDAO.obrisiKomentar(komentar, putanja);
				if (k != null) {
					return Response.ok(k).build();
				}
				return Response.status(400).entity("Greska pri brisanju komentara").build();
			} catch (IOException e) {
				return Response.status(400).entity("Greska pri brisanju komentara").build();
			}
			
		}
		return Response.status(401).entity("Sesija vam je istekla").build();
	}
}
