package service;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dao.KorisnikDAO;
import model.Korisnik;
import session_control.JWTKontroler;

@Path("/izmena")
public class IzmenaPodatakaServis {

	@Context
	ServletContext kontekst;
	
	@PostConstruct
	public void init() {
		if (kontekst.getAttribute("korisnikDAO") == null) {
	    	String putanja = kontekst.getRealPath("");
			kontekst.setAttribute("korisnikDAO", new KorisnikDAO(putanja));
		}
		if (kontekst.getAttribute("jwtKontroler") == null) {
			kontekst.setAttribute("jwtKontroler", new JWTKontroler());
		}
	}
	
	@PUT
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response izmenaPodataka(@Context HttpServletRequest zahtev, Korisnik korisnik) {
		JWTKontroler jwtKontroler = (JWTKontroler) kontekst.getAttribute("jwtKontroler");
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		Korisnik ulogovani = jwtKontroler.proveriJWT(zahtev, korisnikDAO);
		if (ulogovani.getKorisnickoIme().equals(korisnik.getKorisnickoIme())) {
			String putanja = kontekst.getRealPath("");
			try {
				Korisnik k = korisnikDAO.izmenaPodataka(korisnik, putanja);
				if (k == null) {
					return Response.status(400).entity("Greska pri izmeni podataka").build();
				}
				return Response.ok(k).build();
			} catch (Exception e) {
				return Response.status(400).entity("Greska pri izmeni podataka").build();
			}
		}
		return Response.status(401).entity("Sesija vam je istekla").build();
	}
}
