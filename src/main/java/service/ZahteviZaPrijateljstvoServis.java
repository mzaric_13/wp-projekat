package service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dao.KorisnikDAO;
import model.Korisnik;
import model.ZahtevZaPrijateljstvo;
import session_control.JWTKontroler;

@Path("/zahteviZaPrijateljstvo")
public class ZahteviZaPrijateljstvoServis {

	@Context
	ServletContext kontekst;
	
	@PostConstruct
	public void init() {
		if (kontekst.getAttribute("korisnikDAO") == null) {
	    	String putanja = kontekst.getRealPath("");
			kontekst.setAttribute("korisnikDAO", new KorisnikDAO(putanja));
		}
		if (kontekst.getAttribute("jwtKontroler") == null) {
			kontekst.setAttribute("jwtKontroler", new JWTKontroler());
		}
	}
	
	@GET
	@Path("/dobaviZahteve")
	@Produces(MediaType.APPLICATION_JSON)
	public Response dobaviZahteve(@Context HttpServletRequest request) {
		JWTKontroler jwtKontroler = (JWTKontroler) kontekst.getAttribute("jwtKontroler");
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		Korisnik ulogovani = jwtKontroler.proveriJWT(request, korisnikDAO);
		Collection<ZahtevZaPrijateljstvo> zahtevi = korisnikDAO.dobaviTrenutneZahteve(ulogovani);
		if (zahtevi == null) {
			Response.status(400).entity("Gre�ka pri dobavljanju zahteva za prijateljstvo!").build();
		}
		if (ulogovani == null) {
			return Response.status(401).entity("Sesija vam je istekla!").build();
		}
		return Response.ok(zahtevi).build();
	}
	
	@POST
	@Path("/izmeniZahtevZaPrijateljstvo")
	@Produces(MediaType.APPLICATION_JSON)
	public Response izmeniZahtev(@Context HttpServletRequest request, @QueryParam("podaci") String podaci) throws IOException {
		JWTKontroler jwtKontroler = (JWTKontroler) kontekst.getAttribute("jwtKontroler");
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		Korisnik ulogovani = jwtKontroler.proveriJWT(request, korisnikDAO);
		String putanja = kontekst.getRealPath("");
		ArrayList<ZahtevZaPrijateljstvo> zahtevi = korisnikDAO.izmeniZahtev(ulogovani, podaci, putanja);
		if (zahtevi == null) {
			Response.status(400).entity("Gre�ka pri dobavljanju zahteva za prijateljstvo!").build();
		}
		if (ulogovani == null) {
			return Response.status(401).entity("Sesija vam je istekla!").build();
		}
		return Response.ok(zahtevi).build();
	}
}
