package service;

import java.io.IOException;
import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dao.KorisnikDAO;
import model.Korisnik;
import session_control.JWTKontroler;

@Path("/necijiProfil")
public class PodaciNecijegProfilaServis {

	@Context
	ServletContext kontekst;
	
	@PostConstruct
	public void init() {
		if (kontekst.getAttribute("korisnikDAO") == null) {
	    	String putanja = kontekst.getRealPath("");
			kontekst.setAttribute("korisnikDAO", new KorisnikDAO(putanja));
		}
		if (kontekst.getAttribute("jwtKontroler") == null) {
			kontekst.setAttribute("jwtKontroler", new JWTKontroler());
		}
	}
	
	@GET
	@Path("/dobaviKorisnika")
	@Produces(MediaType.APPLICATION_JSON)
	public Response dobaviKorisnika(@Context HttpServletRequest request, @QueryParam("korisnickoIme") String korisnickoIme){
		JWTKontroler jwtKontroler = (JWTKontroler) kontekst.getAttribute("jwtKontroler");
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		Korisnik ulogovani = jwtKontroler.proveriJWT(request, korisnikDAO);
		Korisnik k = korisnikDAO.dobaviKorisnika(korisnickoIme);
		if (ulogovani == null) {
			return Response.status(401).entity("Sesija vam je istekla!").build();
		}
		if (k == null) {
			return Response.status(400).entity("Greška pri dobavljanju korisnika!").build();
		}
		return Response.ok(k).build();
	}
	
	@GET
	@Path("/dobaviKorisnikaBezJwt")
	@Produces(MediaType.APPLICATION_JSON)
	public Response dobaviKorisnikaBezJwt(@Context HttpServletRequest request, @QueryParam("korisnickoIme") String korisnickoIme){
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		Korisnik k = korisnikDAO.dobaviKorisnika(korisnickoIme);
		if (k == null) {
			return Response.status(400).entity("Greška pri dobavljanju korisnika!").build();
		}
		return Response.ok(k).build();
	}
	
	@GET
	@Path("/dobaviZajednickePrijatelje")
	@Produces(MediaType.APPLICATION_JSON)
	public Response dobaviZajednickePrijatelje (@Context HttpServletRequest request, @QueryParam("korisnickoIme") String korisnickoIme) {
		JWTKontroler jwtKontroler = (JWTKontroler) kontekst.getAttribute("jwtKontroler");
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		Korisnik ulogovani = jwtKontroler.proveriJWT(request, korisnikDAO);
		Collection<Korisnik> prijatelji = korisnikDAO.dobaviZajednickePrijatelje(ulogovani, korisnickoIme);
		if (prijatelji == null) {
			return Response.status(400).entity("Greška pri dobavljanju zajedničkih prijatelja!").build();
		}
		if (ulogovani == null) {
			return Response.status(401).entity("Sesija vam je istekla!").build();
		}
		return Response.ok(prijatelji).build();
	}
	
	@GET
	@Path("/proveraPrijateljstva")
	public Response proveraPrijateljstva(@Context HttpServletRequest request, @QueryParam("korisnickoIme") String korisnickoIme) {
		JWTKontroler jwtKontroler = (JWTKontroler) kontekst.getAttribute("jwtKontroler");
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		Korisnik ulogovani = jwtKontroler.proveriJWT(request, korisnikDAO);
		String provera = korisnikDAO.proveraPrijateljstva(ulogovani, korisnickoIme);
		if (provera == null) {
			return Response.status(400).entity("Greška pri proveri prijateljstva!").build();
		}
		if (ulogovani == null) {
			return Response.status(401).entity("Sesija vam je istekla!").build();
		}
		return Response.ok(provera).build();
	}
	@POST
	@Path("izmenaPrijateljstva")
	public Response izmeniZahtevZaPrijateljstvo(@Context HttpServletRequest request,
			@QueryParam("korisnickoIme") String korisnickoIme, @QueryParam("opcija") String opcija) throws IOException {
		JWTKontroler jwtKontroler = (JWTKontroler) kontekst.getAttribute("jwtKontroler");
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		Korisnik ulogovani = jwtKontroler.proveriJWT(request, korisnikDAO);
		String putanja = kontekst.getRealPath("");
		Korisnik k = korisnikDAO.izmenaZahtevaZaPrijateljstvo(ulogovani, korisnickoIme, opcija, putanja);
		if (k == null) {
			return Response.status(400).entity("Greška pri izmeni zahteva za prijateljstvo!").build();
		} 
		if (ulogovani == null) {
			return Response.status(401).entity("Sesija vam je istekla!").build();
		}
		return Response.ok(k).build();
	}
	
}
