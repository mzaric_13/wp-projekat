package service;


import java.text.ParseException;
import java.util.Collection;
import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dao.KorisnikDAO;
import model.Korisnik;
import session_control.JWTKontroler;

@Path("/pretragaKorisnika")
public class PretragaKorisnikaServis {
	
	@Context
	ServletContext kontekst;
	
	@PostConstruct
	public void init() {
		if (kontekst.getAttribute("korisnikDAO") == null) {
	    	String putanja = kontekst.getRealPath("");
			kontekst.setAttribute("korisnikDAO", new KorisnikDAO(putanja));
		}
		if (kontekst.getAttribute("jwtKontroler") == null) {
			kontekst.setAttribute("jwtKontroler", new JWTKontroler());
		}
	}
	
	@GET
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response pretraziKorisnike(@QueryParam("ime") String ime, @QueryParam("prezime") String prezime,
													@QueryParam("pocetniDatum") String pocetniDatum, 
													@QueryParam("krajnjiDatum") String krajnjiDatum) throws ParseException{
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		Collection<Korisnik> korisnici = korisnikDAO.pronadjiKorisnikePoKriterijumima(ime, prezime, pocetniDatum, krajnjiDatum);
		if (korisnici == null) {
			return Response.status(400).entity("Gre�ka pri pretrazi korisnika!").build();
		}
		return Response.ok(korisnici).build();
	}
	
	@GET
	@Path("/ulogovani")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response pretraziKorisnike(@Context HttpServletRequest request, @QueryParam("ime") String ime, 
			@QueryParam("prezime") String prezime,
			@QueryParam("pocetniDatum") String pocetniDatum, 
			@QueryParam("krajnjiDatum") String krajnjiDatum) throws ParseException {
		JWTKontroler jwtKontroler = (JWTKontroler) kontekst.getAttribute("jwtKontroler");
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		Korisnik ulogovani = jwtKontroler.proveriJWT(request, korisnikDAO);
		Collection<Korisnik> korisnici = 
				korisnikDAO.pronadjiKorisnikePoKriterijumimaUlogovani(ulogovani, ime, prezime, pocetniDatum, krajnjiDatum);
		if (korisnici == null) {
			return Response.status(400).entity("Gre�ka pri pretrazi korisnika!").build();
		}
		if (ulogovani == null) {
			return Response.status(401).entity("Sesija vam je istekla!").build();
		}
		return Response.ok(korisnici).build();
	}
	
	@GET
	@Path("/admin")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response pretraziKorisnike(@Context HttpServletRequest request, @QueryParam("ime") String ime,
			@QueryParam("prezime") String prezime, @QueryParam("email") String email) {
		JWTKontroler jwtKontroler = (JWTKontroler) kontekst.getAttribute("jwtKontroler");
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		Korisnik ulogovani = jwtKontroler.proveriJWT(request, korisnikDAO);
		Collection<Korisnik> korisnici =
				korisnikDAO.pronadjiKorisnikeAdmin(ime, prezime, email);
		if (korisnici == null) {
			return Response.status(400).entity("Gre�ka pri pretrazi korisnika!").build();
		}
		if (ulogovani == null) {
			return Response.status(401).entity("Sesija vam je istekla!").build();
		}
		return Response.ok(korisnici).build();
	}
	
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response sortirajKorisnike(@QueryParam("opcija") String opcija, Collection<Korisnik> korisnici) {
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		Collection<Korisnik> sortiraniKorisnici = korisnikDAO.sortirajKorisnike(opcija, korisnici);
		if (sortiraniKorisnici == null) {
			return Response.status(400).entity("Gre�ka pri sortiranju korisnika!").build();
		}
		return Response.ok(sortiraniKorisnici).build();
	}
	
	@POST
	@Path("/sortUlogovani")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response sortirajKorisnike(@Context HttpServletRequest request, @QueryParam("opcija") String opcija, Collection<Korisnik> korisnici) {
		JWTKontroler jwtKontroler = (JWTKontroler) kontekst.getAttribute("jwtKontroler");
		KorisnikDAO korisnikDAO = (KorisnikDAO) kontekst.getAttribute("korisnikDAO");
		Korisnik ulogovani = jwtKontroler.proveriJWT(request, korisnikDAO);
		Collection<Korisnik> sortiraniKorisnici = korisnikDAO.sortirajKorisnike(opcija, korisnici);
		if (sortiraniKorisnici == null) {
			return Response.status(400).entity("Gre�ka pri sortiranju korisnika!").build();
		}
		if (ulogovani == null) {
			return Response.status(401).entity("Sesija vam je istekla!").build();
		}
		return Response.ok(sortiraniKorisnici).build();
	}
}

