package socket;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/websocket")
public class SocketPoruke {
	
	private static List<Session> sesije = new ArrayList<Session>();
	
	@OnOpen
	public void otvoriSesiju(Session sesija) {
		if (!sesije.contains(sesija)) {
			sesije.add(sesija);
		}
	}
	
	@OnMessage
	public void primljenaPoruka(Session sesija, String poruka) {
		try {
			if (sesija.isOpen()) { 
				for (Session s : sesije) {
					s.getBasicRemote().sendText(poruka);
		    	} 
		    }
		} 
		catch (IOException e) {
			try {
				sesija.close();
			} 
			catch (IOException e1) {
				
			}
		}

	}
	
	@OnClose
	public void zatvoriKanal(Session sesija) {
		sesije.remove(sesija);
	}

}
