function dobaviOsnovneInformacije(korisnik){
	let h2ImePrezime = $('<h2><center>Dobrodošli, ' + korisnik.ime + " " + korisnik.prezime + '</center></h2>'); 
	let imgProfilna = $('<img src="' + korisnik.profilnaSlika + '" class="rounder mx-auto slika rounded-circle fiksiraj_profilnu" alt="Responsive image" id="profilna">');
	$("#osnovni_podaci1").append(h2ImePrezime)
						.append(imgProfilna);
						
	let pPol = $('<p id="prvi"><center> Pol: ' + korisnik.pol + '</center></p>');
	let datumRodjenja = new Date(korisnik.datumRodjenja);
    let pDatumRodjenja = $('<p><center> Datum rođenja: ' + datumRodjenja.toLocaleDateString() + '</center></p>');
    let pKorisnickoIme = $('<p><center> Korisničko ime: ' + korisnik.korisnickoIme + '</center></p>');
    let pEmailAdresa = $('<p><center> E-mail adresa: ' +  korisnik.email + '</center></p>');    
    let pHref = $('<a href="izmena_podataka.html" class="link-info"><center>Izmeni podatke</center></a>');
	$("#osnovni_podaci2").append(pPol)
						.append(pDatumRodjenja)
						.append(pKorisnickoIme)
						.append(pEmailAdresa)
						.append(pHref);
}

$(document).ready(function(){
	$.get({
		url: "../rest/osnovniPodaci/dobaviKorisnika",	
		headers:{'Authorization':'Bearer ' + sessionStorage.getItem('jwt')},
		contentType:"application/json",
		dataType:"json",
		success: function(korisnik){
			dobaviOsnovneInformacije(korisnik);
		},
		error: function(){
			location.assign("../html/login.html");
		}
	});
	
	$('#promenaProfilne').click(function(){
		let slika = $('input[name="novaProfilna"]').val().split('\\')[2];
		$.post({
			url: "../rest/osnovniPodaci/promeniProfilnu?slika=" + slika,
			headers:{'Authorization':'Bearer ' + sessionStorage.getItem('jwt')},
			contentType:"application/json",
			dataType:"json",
			success: function(korisnik){
				alert("Uspešno promenjena profilna slika korisniku " + korisnik.ime + " " + korisnik.prezime);
				location.reload();
			},
			error: function(){
				alert("Došlo je do greške.")
			}
		})
	});
	$("#logout").bind("click", function(event){
		event.preventDefault();
		$.get({
			url: "../rest/prijava/odjava",
			headers:{'Authorization':'Bearer ' + sessionStorage.getItem('jwt')},
			contentType:"application/json",
			dataType:"json",
			success: function(korisnik){
				sessionStorage.removeItem("jwt");
				location.assign("../html/login.html");
			},
			error: function(){
				alert("Greška");
			}
		})
	})
})