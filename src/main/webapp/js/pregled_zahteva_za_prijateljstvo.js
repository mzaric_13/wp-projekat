function iscrtajRed(zahtev){
	let red = $('<tr> </tr>');
	let tdProfilnaSlika = $('<td><img src="' + zahtev.posiljalac.profilnaSlika +'" alt=""'+
	'class="rounded-circle fiksiraj_profilne"></td>');
	let tdImePrezime = $('<td>'+ zahtev.posiljalac.ime + ' ' + zahtev.posiljalac.prezime +'</td>');
	
	let tdButtonGreen = $('<td> </td>');
	//id za prihvatanje korisnika : prihvati_korisnickoIme
	let buttonGreen = $('<button type="button" class="btn btn-success" id="prihvati_' + 
	zahtev.posiljalac.korisnickoIme +'"><i class="fas fa-check"></i></button>');
	tdButtonGreen.append(buttonGreen);
	
	let tdButtonRed = $('<td> </td>');
	//id za odbijanje korisnika : odbij_korisnickoIme
	let buttonRed = $('<button type="button" class="btn btn-danger" id="odbij_' + 
	zahtev.posiljalac.korisnickoIme +'"><i class="fas fa-times"></i></button>');
	tdButtonRed.append(buttonRed);
	
	red.append(tdProfilnaSlika)
		.append(tdImePrezime)
		.append(tdButtonGreen)
		.append(tdButtonRed);
		
	$("table").append(red);
		
}

function popuniTabeluZahtevima(zahtevi){
	$("table").empty();
	for (let zahtev of zahtevi){
		iscrtajRed(zahtev);
	}
	let buttons = $("table tr td button");
	for (let b of buttons){
		$(b).bind("click", function(event){
			let id = $(this).attr("id");
			$.post({
				url : "../rest/zahteviZaPrijateljstvo/izmeniZahtevZaPrijateljstvo?podaci=" + id,
				headers: {'Authorization':'Bearer ' + sessionStorage.getItem('jwt')},
				contentType: "application/json",
				dataType: "json",
				success: function(zahtevi){
					popuniTabeluZahtevima(zahtevi);
				},
				error: function(){
					alert("Greška!");
				}
			});
		});
	}
}

$(document).ready(function(){
	$.get({
		url: "../rest/zahteviZaPrijateljstvo/dobaviZahteve",
		headers: {'Authorization':'Bearer ' + sessionStorage.getItem('jwt')},
		contentType:"application/json",
		dataType:"json",
		success: function(zahtevi){
			popuniTabeluZahtevima(zahtevi);
		},
		error: function(){
			alert("Greška.");
		}
	});
});