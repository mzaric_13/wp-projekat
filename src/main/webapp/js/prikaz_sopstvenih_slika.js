function prikazSlika(slike) {
    let i = 0;
    let tabela = $("#tabela");
    var tr = $("<tr></tr>");
    for(let sl of slike) {
		if (sl.obrisan) continue;
        let td = $("<td></td>");
        let img = $('<img src="'+ sl.slika + '" alt="" class="rounded-circle fiksiraj_objave"/>');
        let a = $('<a href="http://localhost:8088/wp-projekat/html/detaljan_prikaz_slike.html?idSopstvena='+ sl.id + '"></a>');
        a.append(img);
        td.append(a);
        tr.append(td);
        i++;
        /*if(i === 3) {
            i = 0;
            tabela.append(tr);
            var tr = $("<tr></tr>");
        }*/
    }
    tabela.append(tr);
}

$(document).ready(function() {
	
    $.get({
        url: "../rest/sopstveneSlike",
        headers: {
	        'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
	    },
		contentType:"application/json",
		dataType:"json",
        success: function(odgovor) {
            prikazSlika(odgovor);
        },
        error: function(odgovor) {
            alert(odgovor.responseText);
            if(odgovor.status === 404) {
                window.location.assign("http://localhost:8088/wp-projekat/html/sopstveni_profil.html");
            }
        }

    });
})