function svaPoljaPrazna(ime, prezime, email){
    if (ime === "" && prezime === "" && email === ""){
        alert("Niste uneli nijedan kriterijum pretraživanja!");
        return true;
    }
    return false;
}

function ispisiOsnovneInformacije(korisnik){
	sessionStorage.setItem("admin", korisnik.ime);
	let pocetak = $('<h2><b> Dobro došli, ' + korisnik.ime + ' ' + korisnik.prezime + '</b></h2>');
	$("#imePrezime").append(pocetak);
}

$(window).bind("pageshow", function(event){
	if (event.originalEvent.persisted){
		location.reload();	
	}
});

$(document).ready(function(){
	$.get({
		url: "../rest/osnovniPodaci/dobaviKorisnika",	
		headers:{'Authorization':'Bearer ' + sessionStorage.getItem('jwt')},
		contentType:"application/json",
		dataType:"json",
		success: function(korisnik){
			ispisiOsnovneInformacije(korisnik);
		},
		error: function(){
			location.assign("../html/login.html");
		}
	});
	
	$("#pretraga_korisnika").submit(function(event){
		event.preventDefault();
	 	let ime = $('input[name="ime"]').val();
		let prezime = $('input[name="prezime"]').val();
		let email = $('input[name="email"]').val();
		if (!svaPoljaPrazna(ime, prezime, email)){
			url = "../rest/pretragaKorisnika/admin?ime=" + ime +
			 "&prezime=" + prezime + "&email=" + email;
			$.get({
				url: url,
				headers:{'Authorization':'Bearer ' + sessionStorage.getItem('jwt')},
				contentType:"application/json",
				dataType:"json", 
				success:function(korisnici){
					sessionStorage.setItem("pretrazeniKorisnici", JSON.stringify(korisnici));
					location.assign("http://localhost:8088/wp-projekat/html/pretrazeni_korisnici.html");
				},
				error:function(){
					alert("Greška!");
				}
			});
		}
		
	});
	
	$("#logout").bind("click", function(event){
		event.preventDefault();
		sessionStorage.removeItem("admin");
		$.get({
			url: "../rest/prijava/odjava",
			headers:{'Authorization':'Bearer ' + sessionStorage.getItem('jwt')},
			contentType:"application/json",
			dataType:"json",
			success: function(korisnik){
				sessionStorage.removeItem("jwt");
				location.assign("../html/login.html");
			},
			error: function(){
				alert("Greška");
			}
		})
	});	
	
	$("#prikaziSveKorisnike").bind("click", function(event){
		$.get({
			url: "../rest/pretragaKorisnika/admin?ime=&prezime=&email=",
			headers:{'Authorization':'Bearer ' + sessionStorage.getItem('jwt')},
			contentType:"application/json",
			dataType:"json", 
			success:function(korisnici){
				sessionStorage.setItem("pretrazeniKorisnici", JSON.stringify(korisnici));
				location.assign("http://localhost:8088/wp-projekat/html/pretrazeni_korisnici.html");
			},
			error:function(){
				alert("Greška!");
			}
		});
	});
	
})