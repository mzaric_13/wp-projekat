function dodajPoslatuPoruku(tekstPoruke, korisnik) {
    let divPolje = $("div#poljePoruke");
    let div = $('<div class="poruka poslata"></div>');
    let slika = $('<img src="' + korisnik.profilnaSlika + '" class="desno" />');
    let por = $('<p>' + tekstPoruke + '</p>');
    div.append(slika).append(por);
    divPolje.append(div);
}

function dodajPrimljenuPoruku(tekstPoruke, prijatelj) {
    let divPolje = $("div#poljePoruke");
    let div = $('<div class="poruka"></div>');
    let slika = $('<img src="' + prijatelj.profilnaSlika + '" />');
    let por = $('<p>' + tekstPoruke + '</p>');
    div.append(slika).append(por);
    divPolje.append(div);
}

function prikaziPoruke(korisnik, prijatelj, poruke) {
    for (poruka of poruke) {
        if (poruka.posiljalac.korisnickoIme === korisnik.korisnickoIme) {
            dodajPoslatuPoruku(poruka.sadrzaj, korisnik);
        }else{
            dodajPrimljenuPoruku(poruka.sadrzaj, prijatelj);
        }
    }
	$('#poljePoruke').scrollTop($('#poljePoruke')[0].scrollHeight);
	if (String(prijatelj.uloga) === "ADMINISTRATOR") {
		return;
	}
	let div = $("div#slanje");
	div.removeAttr("hidden");
}

function dobaviKonverzaciju(korisnik, prijatelj) {
    $.get({
        url: "../rest/poruke?primalac=" + prijatelj.korisnickoIme,
        headers:{
            'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
        },
        contentType: "application/json",
        dataType: "json",
        success: function(odgovor) {
            prikaziPoruke(korisnik, prijatelj, odgovor);
        }
    });
}

function upisiPrijatelje(korisnik, prijatelji) {
    for (let prijatelj of prijatelji) {
		if ($("#" + prijatelj.korisnickoIme).length) continue;
        upisiPrijatelja(korisnik, prijatelj);
    }
}

function dobaviPrijatelje(korisnik) {
    $.get({
        url: "../rest/osnovniPodaci/dobaviPrijatelje",
        headers:{
            'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
        },
        contentType: "application/json",
        dataType: "json",
        success: function(odgovor) {
            upisiPrijatelje(korisnik, odgovor);
        }
    });
}

function upisiPrijatelja(korisnik, prijatelj) {
	let divPrijatelji = $('div#prijatelji');
	let div = $('<div id="' + prijatelj.korisnickoIme + '" class="poljePrijatelj"></div>');
    let slika = $('<img src="' + prijatelj.profilnaSlika + '" class="rounded-circle profilna" />');
	if (String(prijatelj.uloga) === "ADMINISTRATOR") {
		div.text(prijatelj.ime + ' ' + prijatelj.prezime + "(ADMIN)");
	}else{
		div.text(prijatelj.ime + ' ' + prijatelj.prezime);	
	}
	div.append(slika);
    div.click(function(event) {
        event.preventDefault();
		$("div#poljePoruke").empty();
		$("div#slanje").attr("hidden", true);
		primalac = div.attr("id");
        dobaviKonverzaciju(korisnik, prijatelj);
    });
    divPrijatelji.append(div);
}

function dobaviCetove(korisnik, poruke) {
	let prijateljiCet = [];
	for (let poruka of poruke) {
		if (poruka.posiljalac.korisnickoIme === korisnik.korisnickoIme){
			if (!prijateljiCet.includes(poruka.primalac.korisnickoIme)) {
				upisiPrijatelja(korisnik, poruka.primalac);
				prijateljiCet.push(poruka.primalac.korisnickoIme);
			}
		}else {
			if (!prijateljiCet.includes(poruka.posiljalac.korisnickoIme)) {
				upisiPrijatelja(korisnik, poruka.posiljalac);
				prijateljiCet.push(poruka.posiljalac.korisnickoIme);
			}
		}
	}
	dobaviPrijatelje(korisnik);
}

function dobaviPoruke(korisnik) {
	$.get({
        url: "../rest/poruke/korisnik",
        headers:{
            'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
        },
        contentType: "application/json",
        dataType: "json",
        success: function(odgovor) {
            dobaviCetove(korisnik, odgovor);
			return;
        },
    });
}

var primalac;
var socket;
$(document).ready(function() {

	let host = "ws://localhost:8088/wp-projekat/websocket";
	socket = new WebSocket(host);
	
	dodajDogadajZaSlanje();
    
	$.get({
        url: "../rest/prijava/provera",
        headers:{
            'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
        },
        contentType: "application/json",
        dataType: "json",
        success: function(odgovor) {
			dobaviPoruke(odgovor);
			const url = new URLSearchParams(window.location.search);
			if(url.has('korisnickoIme')) {
				promeniCet(odgovor, url.get("korisnickoIme"), "ne");
			}
			if (url.has('prijatelj')) {
				promeniCet(odgovor, url.get('prijatelj'), "da");
			}
        },
    });

	socket.onopen = function() {
		
	}
	
	socket.onmessage = function(poruka) {
		let deloviPoruke = poruka.data.split('-');
		$.get({
			url: "../rest/prijava/provera",
        	headers:{
            	'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
	        },
	        contentType: "application/json",
	        dataType: "json",
	        success: function(odgovor) {
				if (odgovor.korisnickoIme === deloviPoruke[1]) {
					if (deloviPoruke[0] != primalac){
						promeniCet(odgovor, deloviPoruke[0], "");
					}else {
						$.get({
						url: "../rest/necijiProfil/dobaviKorisnika?korisnickoIme=" + primalac,
						headers:{
			            	'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
				        },
				        contentType: "application/json",
				        dataType: "json",
						success: function(korisnik) {
							dodajPrimljenuPoruku(deloviPoruke[2], korisnik);
						}
					});	
					}
				}
	        }
		});
	}
	
	socket.onclose = function() {
		socket = null;
	}
});

function dodajDogadajZaSlanje() {
	$("#slanjePoruke").click(function(event) {
		event.preventDefault();
		let tekst = $('input[name="tekstPoruke"]').val();
		if (tekst === "") {
			
		}else {
			dobaviKorisnike(tekst);
		}
	});
}

function dobaviKorisnike(tekst) {
	$.get({
		url: "../rest/prijava/provera",
        headers:{
            'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
	    },
	    contentType: "application/json",
	    dataType: "json",
	    success: function(odgovor) {
			$.get({
				url: "../rest/necijiProfil/dobaviKorisnika?korisnickoIme=" + primalac,
				headers:{
			           'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
				},
				contentType: "application/json",
				dataType: "json",
				success: function(korisnik) {
					posaljiPoruku(odgovor, korisnik, tekst);
				}
			});
	    }
	})
}

function posaljiPoruku(posiljalac, korisnikPrimalac, tekst) {
	let poruka = "";
	poruka += posiljalac.korisnickoIme + "-";
	poruka += korisnikPrimalac.korisnickoIme + "-";
	poruka += tekst;
	socket.send(poruka);
	$.post({
		url:"../rest/poruke",
		headers:{
			   'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
		},
		contentType: "application/json",
		dataType: "json",
		data: JSON.stringify({
			posiljalac: posiljalac,
			primalac: korisnikPrimalac, 
			sadrzaj: tekst,
			datumSlanja: Date.now()
		}),
		success: function(odgovor) {
			dodajPoslatuPoruku(odgovor.sadrzaj, posiljalac);
		}
	});
	$('input[name="tekstPoruke"]').val("");
}

function promeniCet(korisnik, korisnickoIme, potvrda) {
	primalac = korisnickoIme;
	$("div#poljePoruke").empty();
	$("div#slanje").attr("hidden", true);
	dobaviKorisnika(korisnik, primalac, potvrda);
}

function dobaviKorisnika(korisnik, korisnickoIme, potvrda) {
	$.get({
		url: "../rest/necijiProfil/dobaviKorisnika?korisnickoIme=" + korisnickoIme,
		headers:{
			   'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
		},
		contentType: "application/json",
		dataType: "json",
		success: function(odgovor) {
			if (potvrda === "da") {
				dobaviKonverzaciju(korisnik, odgovor);
			}else {
				prikaziZaKorisnika(korisnik, odgovor);	
			}
		}
	});
}

function prikaziZaKorisnika(korisnik, prijatelj) {
	if ($("div#" + prijatelj.korisnickoIme).length > 0) {
		dobaviKonverzaciju(korisnik, prijatelj);
	}else {
		upisiPrijatelja(korisnik, prijatelj);
		dobaviKonverzaciju(korisnik, prijatelj);
	}
}