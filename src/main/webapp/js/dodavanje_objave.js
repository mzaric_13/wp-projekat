$(document).ready(function() {
	
	const url = new URLSearchParams(window.location.search);
	
	if(!url.has("foto")){
		$("#forma").submit(function(event) {
	        event.preventDefault();
	
	        let tekst = $('textarea[name="opisSlike"]').val();
	        let slika = $('input[name="slika"]').val().split('\\')[2];
	
	        let tdGreska = $("#greska");
	        if (tekst === "") {
	            tdGreska.text("Niste uneli opis objave!");
	            tdGreska.removeAttr("hidden");
	        }else{
	            tdGreska.attr("hidden", true);
	
	            $.post({
	                url: "../rest/sopstveneObjave/dodajObjavu",
	                headers: {
	                    'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
	                },
	                contentType:"application/json",
	                dataType:"json",
	                data: JSON.stringify({
	                    slika: slika,
	                    tekst: tekst,
	                    obrisan: false
	                }),
	                success: function(odgovor) {
	                    alert("Uspesno dodata objava");
	                    window.location.assign("http://localhost:8088/wp-projekat/html/sopstveni_profil.html");
	                },
	                error: function(odgovor) {
	                    alert(odgovor.responseText);
	                }
	            });
	        }
	    });		
	}


})