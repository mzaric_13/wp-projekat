function popuniPolja(korisnik) {
    $('input[name="korisnickoIme"]').val(korisnik.korisnickoIme);
    $('input[name="email"]').val(korisnik.email);
    $('input[name="ime"]').val(korisnik.ime);
    $('input[name="prezime"]').val(korisnik.prezime);
    $('select[name="pol"]').val(korisnik.pol);
    $('input#datum').val(korisnik.datumRodjenja);
    $('select[name="privatanNalog"]').val(String(korisnik.privatanNalog));
}

function prikaziPolja() {
    $("#staraLab").removeAttr("hidden");
    $("#lozinkaLab").removeAttr("hidden");
    $("#potvrdaLab").removeAttr("hidden");
    $('input[name="staraLozinka"]').removeAttr("hidden");
    $('input[name="lozinka"]').removeAttr("hidden");
    $('input[name="potvrdaLozinke"]').removeAttr("hidden");
    $('input[name="promenaLozinke"]').attr("hidden", true);
}

function validacijaPodataka(email, ime, prezime, datumRodjenja) {
    if (!validacijaEmail(email) || !validacijaImePrezime(ime, "#greskaIme", "ime") || !validacijaImePrezime(prezime, "#greskaPrezime", "prezime")  
        || !validacijaDatumRodjenja(datumRodjenja)) {
            return false;
    }
    return true;
}

function promenaLozinke(odgovor, staraLozinka, lozinka, potvrdaLozinke) {
    let tdGreska = $("#greskaStara");
    if(staraLozinka === odgovor.lozinka) {
        tdGreska.attr("hidden", true);
        if(validacijaNeunetoPolje(lozinka, "#greskaLozinka", "Niste uneli novu lozinku!") && validacijaNeunetoPolje(potvrdaLozinke, "#greskaPotvrdaLozinke", "Niste potvrdili lozinku!")){
            if (!proveraLozinka(lozinka, potvrdaLozinke)) {
                tdGreska.text("Uneta potvrda lozinke se ne poklapa sa zadatom lozinkom!");
                tdGreska.removeAttr("hidden");
                return false;
            }else{
                tdGreska.attr("hidden", true);
                return true;
            }
        }else {
            return false;
        }
    }else {
        tdGreska.text("Niste uneli ispravnu trenutnu lozinku!");
        tdGreska.removeAttr("hidden");
        return false;
    }
}

function slanjeZahtevaZaPromenu(korisnickoIme, email, ime, prezime, pol, privatanNalog, datumRodjenja, lozinka, profilnaSlika) {
    $.ajax({
        type: "PUT",
        url: "../rest/izmena",
        headers: {
            'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
        },
        contentType:"application/json",
        dataType:"json",
        data: JSON.stringify({
            korisnickoIme: korisnickoIme,
            email: email,
            ime: ime,
            prezime: prezime,
            pol: pol,
            datumRodjenja: datumRodjenja,
            privatanNalog: privatanNalog,
            lozinka: lozinka,
			profilnaSlika: profilnaSlika
        }),
        success: function(odgovor) {
            alert("Uspešno izmenjeni lični podaci!")
            window.location.assign("http://localhost:8088/wp-projekat/html/sopstveni_profil.html");
        },
        error: function(odgovor) {
			alert(odgovor.responseText);
			if(odgovor.status == 401) {
				sessionStorage.removeItem("jwt");
				history.back();
			}
        }
    });
}

$(document).ready(function() {
    $.get({
        url: "../rest/prijava/provera",
        headers: {
	        'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
	    },
		contentType:"application/json",
		dataType:"json",
        success: function(odgovor) {
            popuniPolja(odgovor);
        }
    });

    $('input[name="promenaLozinke"]').click(function(event) {
        event.preventDefault();
        prikaziPolja();
    });

	$('input[name="promenaProfilne"]').click(function(event){
		$("#fotografije").empty();
		event.preventDefault();
		dobaviFotografije();
	})

    $("#forma_izmena").submit(function(event) {
        event.preventDefault();

        let korisnickoIme = $('input[name="korisnickoIme"]').val();
        let email = $('input[name="email"]').val();
        let ime = $('input[name="ime"]').val();
        let prezime = $('input[name="prezime"]').val();
        let pol = $('select[name="pol"]').find(":selected").val();
        let privatanNalog = $('select[name="privatanNalog"]').find(":selected").val();
        let datumRodjenja = $('input#datum').val();
        let staraLozinka = $('input[name="staraLozinka"]').val();
        let lozinka = $('input[name="lozinka"]').val();
        let potvrdaLozinke = $('input[name="potvrdaLozinke"]').val();
		let profilnaSlika = $('input[name="slika"]:checked').val();
	
		if (!profilnaSlika) {
			profilnaSlika = "";
		}
		
        if(staraLozinka === "" && lozinka === "" && potvrdaLozinke === "") {
            if(validacijaPodataka(email, ime, prezime, datumRodjenja)) {
                slanjeZahtevaZaPromenu(korisnickoIme, email, ime, prezime, pol, privatanNalog, datumRodjenja, lozinka, profilnaSlika);
            }
        }else {
            $.get({
                url: "../rest/prijava/provera",
                headers: {
                    'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
                },
                contentType:"application/json",
                dataType:"json",
                success: function(odgovor) {
                    if(validacijaPodataka(email, ime, prezime, datumRodjenja)) {
                        if(promenaLozinke(odgovor, staraLozinka, lozinka, potvrdaLozinke)) {
                            slanjeZahtevaZaPromenu(korisnickoIme, email, ime, prezime, pol, privatanNalog, datumRodjenja, lozinka, profilnaSlika);
                        }
                    }
                },
                error: function(odgovor) {
                    alert(odgovor.responseText);
                }
            })
        }
    });
});

function dobaviFotografije() {
	$.get({
        url: "../rest/sopstveneSlike",
        headers: {
	        'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
	    },
		contentType:"application/json",
		dataType:"json",
        success: function(odgovor) {
            prikazFotografija(odgovor);
        }
    });
}

function prikazFotografija(fotografije) {
	let postojeceFotografije = [];
	for (let fotografija of fotografije) {
		if (fotografija.obrisan) continue;
		postojeceFotografije.push(fotografija);
	}
	$("table#fotografije").removeAttr("hidden");
	$("p#ispis").removeAttr("hidden");
	for (let i=0; i<postojeceFotografije.length / 3; i++){
		let tr = $('<tr></tr>');
		$("#fotografije").append(tr);
		
		let sledeci = 3 * i;
		for (let j=0; j < 3 && sledeci + j < postojeceFotografije.length; j++){
			let td = $('<td> </td>');
			let label = $('<label></label>');
			let slika = postojeceFotografije[sledeci + j].slika;
			let input = $('<input type="radio" name="slika" value="' + slika + '" /><img src="' + slika + '" alt="" class="rounded-circle fiksiraj_objave" />');
			label.append(input);
			$(td).append(label);
			$(tr).append(td);
		}
	}
}