function upisiKomentar(komentar) {
	var tabela = $("#komentari");
	let tr = $("<tr></tr>");
	var tdSlikaProfila = $('<td><img src="' + komentar.korisnik.profilnaSlika + '" alt="" class="rounded-circle fiksiraj_profilne" /></td>');
	var tdKorisnik = $('<td>' + komentar.korisnik.ime + ' ' + komentar.korisnik.prezime + '</td>');
	let tdOpis = $('<td>' + komentar.tekst + '</td>');
	let dugmeBrisanje = $('<i id="brisanjeKomentara" class="bi bi-trash-fill"></i>');
	let tdBrisanje = $('<td></td>');
	tdBrisanje.append(dugmeBrisanje);
	tr.append(tdSlikaProfila).append(tdKorisnik).append(tdOpis).append(tdBrisanje);
	tabela.append(tr);
	
	$(dugmeBrisanje).click(function(event) {
		event.preventDefault();
		$.ajax({
			type: "PUT",
			url: "../rest/sopstveneObjave/brisanjeKomentar",
			headers: {
	        	'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
	    	},
			contentType:"application/json",
			dataType:"json",
			data: JSON.stringify(komentar),
			success: function(odgovor){
				alert("Komentar: " + odgovor.tekst + ", je obrisan!");
				window.location.reload();
			},
			error: function(odgovor) {
				alert(odgovor.responseText);
			}
		});
	});
}

function upisiKomentare(komentari) {
	for(let kom of komentari) {
		if(kom.obrisan === true) continue;
		upisiKomentar(kom);
	}
}

function dobaviKomentare(objava) {
	$.get({
        url: "../rest/sopstveneObjave/komentari?id=" + objava.id,
        headers: {
            'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
        },
        contentType:"application/json",
        dataType:"json",
        success: function(odgovor) {
            upisiKomentare(odgovor);
        },
        error: function(odgovor) {
            alert(odgovor.responseText);
        }
    });
}

function reakcijaBrisanje(objava) {
	$('i#brisanjeObjave').click(function(event) {
		event.preventDefault();
		
		$.ajax({
			type: "PUT",
			url: "../rest/sopstveneObjave/brisanje",
			headers: {
	        	'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
	    	},
			contentType:"application/json",
			dataType:"json",
			data: JSON.stringify(objava),
			success: function(odgovor){
				alert("Objava uspesno obrisana");
				history.back();
			},
			error: function(odgovor) {
				alert(odgovor.responseText);
			}
		});
	});
}

function prikazSaSlikom(objava) {
	var divPost = $("div#post");
	let slika = $('<img src="' + objava.slika + '" id="slikaObjave"/></td>');
	divPost.append(slika);
    $("#tekst").text(objava.tekst);
	let dugmeBrisanje = $('<i id="brisanjeObjave" class="bi bi-trash-fill"></i>');
	$("#tekst").append(dugmeBrisanje);
	
	reakcijaBrisanje(objava);
	dobaviKomentare(objava);
}

function prikazBezSlike(objava) {
	var divPost = $("div#post");
	divPost.css("width", "500 px");
	divPost.css("height", "200 px");
	var divTekst = $("div#tekst");
	divTekst.css("width", "100%");
	let dugmeBrisanje = $('<i id="brisanjeObjave" class="bi bi-trash-fill"></i>');
	divTekst.text(objava.tekst);
	divTekst.append(dugmeBrisanje);
	reakcijaBrisanje(objava);
	dobaviKomentare(objava);
} 

function prikazObjave(objava) {
	if (objava.slika) {
		prikazSaSlikom(objava);
	}else {
		prikazBezSlike(objava);
	}	
}

$(document).ready(function() {

    const url = new URLSearchParams(window.location.search);

	if(url.has('idSopstvena')) {
		var id = url.get('idSopstvena');
		$.get({
        	url: "../rest/sopstveneObjave/objava?id=" + id,
        	headers: {
	        	'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
	    	},
			contentType:"application/json",
			dataType:"json",
	        success: function(odgovor) {
	            prikazObjave(odgovor);
	        },
	        error: function(odgovor) {
	            alert(odgovor.responseText);
	        }
    	});	
	}
})