function prikazPodataka(korisnik){
	$("#profilna").empty();
	$("#osnovniPodaci").empty();
	$("#prijatelji").empty();
	$("#slike").empty();
	$("#objave").empty();
	let profilna = $('<center><img src="' + korisnik.profilnaSlika + '" alt="" class="rounded-circle fiksiraj_velicinu_profilne"></center>');
	$("#profilna").append(profilna);
	let imePrezime = ('<h2><b><center>' + korisnik.ime + ' ' + korisnik.prezime + '</center></b></h2>');
	let datum = new Date(korisnik.datumRodjenja);
	let datumRodjenja = $('<h4><center>Rođen: ' + datum.toLocaleDateString() + '</center></h4>');
	$("#osnovniPodaci").append(imePrezime).append(datumRodjenja);
	if (sessionStorage.getItem('jwt') && !sessionStorage.getItem("admin")){
		$.get({
			url: "../rest/necijiProfil/proveraPrijateljstva?korisnickoIme=" + korisnik.korisnickoIme,
			headers: {
				'Authorization': 'Bearer ' + sessionStorage.getItem('jwt')
			},
			success: function(potvrda){
				console.log(potvrda);
				if (potvrda === "da"){
					dobaviZajednickePrijatelje(korisnik, potvrda);
				}else if ((potvrda === "ne" || potvrda === "mozda") && korisnik.privatanNalog){
					prikaziDugmad(korisnik, potvrda);
				} else {
				dobaviSlike(korisnik, potvrda);					
				}
			},
			error: function(){
				alert("greska");
			}
		});
	}else if((!sessionStorage.getItem('jwt') && !korisnik.privatanNalog) || sessionStorage.getItem('admin')){		 
		dobaviSlike(korisnik, "neregularanKorisnik");
	}
}

function dobaviZajednickePrijatelje(korisnik, potvrda){
	$.get({
			url: "../rest/necijiProfil/dobaviZajednickePrijatelje?korisnickoIme=" + korisnik.korisnickoIme,
			headers:{
				'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
			},
			contentType: "application/json",
			dataType: "json",
			success: function(zajednickiPrijatelji){
				if (zajednickiPrijatelji.length > 0){
					prikazZajednickihPrijatelja(korisnik, zajednickiPrijatelji, potvrda);	
				} else {
					dobaviSlike(korisnik, potvrda);
				}
			},
			error: function(){
				alert("greska");
			}
	});	
}

function prikazZajednickihPrijatelja(korisnik, zajednickiPrijatelji, potvrda){
	if (sessionStorage.getItem('jwt')){
		let natpis = $("<h3><b><center>Zajednički prijatelji</center></b></h3>");
		$("#zajednicki").append(natpis);
	}
	for (let i=0; i<zajednickiPrijatelji.length / 2; i++){
		let nextRow = $('<tr></tr>');
		$("#prijatelji").append(nextRow);
	
		let nextItem = 2 * i;
		for (let j=0; j < 2 && nextItem + j < zajednickiPrijatelji.length; j++){
			let tdNextCell = $('<td> </td>');		
			let hrefSlika = $('<a href="http://localhost:8088/wp-projekat/html/neciji_profil.html?korisnickoIme='
			 + zajednickiPrijatelji[nextItem + j].korisnickoIme + '"><img src="'
			 + zajednickiPrijatelji[nextItem + j].profilnaSlika + 
			 '" alt="" class="rounded-circle fiksiraj_velicinu_prijatelja"></a>');
			$(tdNextCell).append(hrefSlika);
			$(nextRow).append(tdNextCell);
		}
	}
	dobaviSlike(korisnik, potvrda);
}

function dobaviSlike(korisnik, potvrda){
	if (sessionStorage.getItem('jwt')){
		$.get({
			url: "../rest/drugeSlike?korisnickoIme=" + korisnik.korisnickoIme,
			headers: {
		       	'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
		    },
			contentType: "application/json",
			dataType: "json",
			success: function(odgovor) {
				prikaziSlike(odgovor, korisnik, potvrda);
			},
			error: function(odgovor){
				alert(odgovor.responseText);
			}
		});
	}else{
		$.get({
			url: "../rest/drugeSlike/bezJwt?korisnickoIme=" + korisnik.korisnickoIme,
			contentType: "application/json",
			dataType: "json",
			success: function(odgovor) {
				prikaziSlike(odgovor, korisnik, potvrda);
			},
			error: function(odgovor){
				alert(odgovor.responseText);
			}
		});
	}
}

function prikaziSlike(dobavljeneSlike, korisnik, potvrda){
	//<td><a href="necija_objava_detaljnije.html"><img src="necijiProfil.png" alt="" class="rounded-circle fiksiraj_velicinu_objave"></a></td>

	let postojeceSlike = [];
	for (let slika of dobavljeneSlike) {
		if (slika.obrisan) continue;
		postojeceSlike.push(slika);
	}
	for (let i=0; i<postojeceSlike.length / 3; i++){
		let nextRow = $('<tr></tr>');
		$("#slike").append(nextRow);
		
		let nextItem = 3 * i;
		for (let j=0; j < 3 && nextItem + j < postojeceSlike.length; j++){
			let tdNextCell = $('<td> </td>');
			let hrefSlika;
			if ((sessionStorage.getItem('jwt') && potvrda === "da") ||
			(sessionStorage.getItem('jwt') && !korisnik.privatanNalog) || sessionStorage.getItem("admin")){
				if (!postojeceSlike[nextItem + j].obrisan){
					hrefSlika = $('<a href="http://localhost:8088/wp-projekat/html/detaljan_prikaz_slike.html?idNecija='
				 	+ postojeceSlike[nextItem + j].id + '"><img src="'
				 	+ postojeceSlike[nextItem + j].slika + 
				 	'" alt="" class="rounded-circle fiksiraj_velicinu_fotografije"></a>');
				}
			}else{
				hrefSlika = $('<img src="' + postojeceSlike[nextItem + j].slika + 
				'" alt="" class="rounded-circle fiksiraj_velicinu_fotografije">');
			}
			$(tdNextCell).append(hrefSlika);
			$(nextRow).append(tdNextCell);
		}
	}
	dobaviObjave(korisnik, potvrda);	
}

function dobaviObjave(korisnik, potvrda){
	if (sessionStorage.getItem('jwt')){
		$.get({
			url: "../rest/drugeObjave?korisnickoIme=" + korisnik.korisnickoIme,
			headers: {
		       	'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
		    },
			contentType: "application/json",
			dataType: "json",
			success: function(odgovor) {
				prikaziObjave(odgovor, korisnik, potvrda);
			},
			error: function(odgovor){
				alert(odgovor.responseText);
			}
		});		
	}else{
		$.get({
			url: "../rest/drugeObjave/bezJwt?korisnickoIme=" + korisnik.korisnickoIme,
			contentType: "application/json",
			dataType: "json",
			success: function(odgovor) {
				prikaziObjave(odgovor, korisnik, potvrda);
			},
			error: function(odgovor){
				alert(odgovor.responseText);
			}
		});			
	}

}

function prikaziObjave(dobavljeneObjave, korisnik, potvrda){
	var divObjave = $("div#objave");
	for (let objava of dobavljeneObjave) {
		if (objava.obrisan) continue;
		let div = $('<div id="' + objava.id + '" class="objava"></div>');
		let divProfil = $('<div id="profil"></div>');
		let profilna = $('<img class="profilnaSlika" src="' + korisnik.profilnaSlika + '" />');
		divProfil.text(korisnik.ime + ' ' + korisnik.prezime);
		divProfil.append(profilna);
		divProfil.css("color", "blue");
		let opis = $('<div id="opis">' + objava.tekst + '</div>');
		div.append(divProfil).append(opis);
		if (objava.slika) {
			let	sl = $('<img id="slika" class="fiksiraj_objave" src="' + objava.slika + '" />');
			div.append(sl);
		}
		if ((sessionStorage.getItem('jwt') && potvrda === "da") ||
			(sessionStorage.getItem('jwt') && !korisnik.privatanNalog) || sessionStorage.getItem("admin")){
			div.click(function(event) {
				event.preventDefault();
				
				window.location.assign("http://localhost:8088/wp-projekat/html/detaljan_prikaz_objave.html?idNecija=" + objava.id);
			});
		}
		divObjave.append(div);
	}
	if (!sessionStorage.getItem("admin")){
		prikaziDugmad(korisnik, potvrda);
	}
	else
	{
		let buttonAdmin = $('<button type="button" class="btn btn-primary" id="nazad">Nazad</button>')
		$("#admin").append(buttonAdmin);
		$(buttonAdmin).bind("click", function(event){
			event.preventDefault();
			window.location.assign("../html/pocetna_admin.html");
		})
	}
}

function prikaziDugmad(korisnik, potvrda){
	//dodati akciju na click
	if (sessionStorage.getItem('jwt')){
		let buttonCaskanje = $('<button type="button" class="btn btn-primary"'+
		' id="slanjePoruke"><i class="far fa-comments"></i>');
		$("#caskanje").append(buttonCaskanje);
		if (!sessionStorage.getItem("admin")){
			let opcija;
			let buttonPrijateljstvo;
			if (potvrda === "da"){
				buttonPrijateljstvo = $(' <button type="button" class="btn btn-primary"'+
				' id="slanjeZahtevaZaPrijateljstvo"><i class="fas fa-user-minus"></i></button>');
				opcija = "izbrisi";
			} else if (potvrda === "mozda"){
				buttonPrijateljstvo = $(' <button type="button" class="btn btn-primary"'+
				' id="slanjeZahtevaZaPrijateljstvo" disabled><i class="fas fa-user"></i></button>');
			} else if (potvrda === "ne") {
				buttonPrijateljstvo = $(' <button type="button" class="btn btn-primary"'+
				' id="slanjeZahtevaZaPrijateljstvo"><i class="fas fa-user-plus"></i></button>');
				opcija = "dodaj";
			}
			$("#prijateljstvo").append(buttonPrijateljstvo);
			$(buttonPrijateljstvo).bind("click", function(event){
				event.preventDefault();
				$.post({
					url: "../rest/necijiProfil/izmenaPrijateljstva?korisnickoIme=" + korisnik.korisnickoIme + "&opcija=" + opcija,
					headers: {
	       				'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
	    			},
					contentType: "application/json",
					dataType: "json",
					success: function(){
						window.location.reload();
					},
					error: function(){
						alert("greska");
					}
				});
			});
		}
	}
	dodajAkcijuCaskanje(korisnik, potvrda);
}

function dodajAkcijuCaskanje(korisnik, potvrda) {
	$("button#slanjePoruke").click(function(event) {
		event.preventDefault();
		if (potvrda === "da") {
			window.location.assign("http://localhost:8088/wp-projekat/html/poruke.html?prijatelj=" + korisnik.korisnickoIme);	
		}else {
			window.location.assign("http://localhost:8088/wp-projekat/html/poruke.html?korisnickoIme=" + korisnik.korisnickoIme);	
		}
	});
}


$(document).ready(function(){
	const queryString = location.search;
	const urlParams = new URLSearchParams(queryString);
	let korisnickoIme = urlParams.get("korisnickoIme");
	pronadjiKorisnika(korisnickoIme);
	
})

function pronadjiKorisnika(korisnickoIme){
	if (sessionStorage.getItem('jwt')){
		$.get({
			url: "../rest/necijiProfil/dobaviKorisnika?korisnickoIme=" + korisnickoIme,
			headers: {
	            'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
	        },
	        contentType:"application/json",
	        dataType:"json",
			success: function(korisnik){
				prikazPodataka(korisnik);
			},
			error: function(){
				alert("greska");
			}
		});
	}
	else{
		$.get({
			url: "../rest/necijiProfil/dobaviKorisnikaBezJwt?korisnickoIme=" + korisnickoIme,
	        contentType:"application/json",
	        dataType:"json",
			success: function(korisnik){
				prikazPodataka(korisnik);
			},
			error: function(){
				alert("greska");
			}
		});
	}
}