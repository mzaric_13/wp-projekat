function iscrtajPrijatelje(prijatelji){
	for (let i=0; i<prijatelji.length / 2; i++){
		let nextRow = $('<tr></tr>');
		$("#prijatelji").append(nextRow);
		
		let nextItem = 2 * i;
		for (let j=0; j < 2 && nextItem + j < prijatelji.length; j++){
			let tdNextCell = $('<td> </td>');
			let hrefSlika = $('<a href="http://localhost:8088/wp-projekat/html/neciji_profil.html?korisnickoIme='
			 + prijatelji[nextItem + j].korisnickoIme + '"><img src="'
			 + prijatelji[nextItem + j].profilnaSlika + 
			 '" alt="" class="rounded-circle fiksiraj_velicinu_prijatelja"></a>');
		//<a href="neciji_profil.html?korisnickoIme=nekoKorisnickoIme"><img src="slika.png" alt=""
		//class="rounded-circle fiksiraj_velicinu_prijatelja"></a> <-- hrefSlika
			$(tdNextCell).append(hrefSlika);
			$(nextRow).append(tdNextCell);
		}
	}
}

$(window).bind("pageshow", function(event){
	if (event.originalEvent.persisted){
		location.reload();	
	}
});

$(document).ready(function(){
	$.get({
		url: "../rest/osnovniPodaci/dobaviPrijatelje",
		headers:{'Authorization':'Bearer ' + sessionStorage.getItem('jwt')},
		contentType:"application/json",
		dataType: "json",
		success: function(prijatelji){
			iscrtajPrijatelje(prijatelji);
		},
		error: function(greska){
			alert(greska.responseText);
		}
	});
});