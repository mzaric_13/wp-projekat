function upisiKomentar(komentar) {
	var tabela = $("#komentari");
	let tr = $("<tr></tr>");
	var tdSlikaProfila = $('<td><img src="' + komentar.korisnik.profilnaSlika + '" alt="" class="rounded-circle fiksiraj_profilne" /></td>');
	var tdKorisnik = $('<td>' + komentar.korisnik.ime + ' ' + komentar.korisnik.prezime + '</td>');
	let tdOpis = $('<td>' + komentar.tekst + '</td>');
	let dugmeBrisanje = $('<i id="brisanjeKomentara" class="bi bi-trash-fill"></i>');
	let tdBrisanje = $('<td></td>');
	tdBrisanje.append(dugmeBrisanje);
	tr.append(tdSlikaProfila).append(tdKorisnik).append(tdOpis).append(tdBrisanje);
	tabela.append(tr);
	
	$(dugmeBrisanje).click(function(event) {
		event.preventDefault();
		$.ajax({
			type: "PUT",
			url: "../rest/sopstveneSlike/brisanjeKomentar",
			headers: {
	        	'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
	    	},
			contentType:"application/json",
			dataType:"json",
			data: JSON.stringify(komentar),
			success: function(odgovor){
				alert("Komentar: " + odgovor.tekst + ", je obrisan!");
				window.location.reload();
			},
			error: function(odgovor) {
				alert(odgovor.responseText);
			}
		});
	});

}


function upisiKomentare(fotografija) {
	for(let kom of fotografija.komentari) {
		if(kom.obrisan === true) continue;
		upisiKomentar(kom)
	}
}

function reakcijaBrisanje(fotografija) {
	$('i#brisanjeFotografije').click(function(event) {
		event.preventDefault();
		
		$.ajax({
			type: "PUT",
			url: "../rest/sopstveneSlike/brisanje",
			headers: {
	        	'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
	    	},
			contentType:"application/json",
			dataType:"json",
			data: JSON.stringify(fotografija),
			success: function(odgovor){
				alert("Slika uspesno obrisana");
				history.back();
			},
			error: function(odgovor) {
				alert(odgovor.responseText);
			}
		});
	});
}

function prikazSaOpisom(fotografija) {
	var divPost = $("div#post");
	let slika = $('<img src="' + fotografija.slika + '" id="slika"/></td>');
	divPost.append(slika);
	let divOpis = $('<div id="opis"></div>');
    divOpis.text(fotografija.opis);
	let dugmeBrisanje = $('<i id="brisanjeFotografije" class="bi bi-trash-fill"></i>');
	divOpis.append(dugmeBrisanje);
	divPost.append(divOpis);
	reakcijaBrisanje(fotografija);
	upisiKomentare(fotografija);
}

function prikazBezOpisa(fotografija) {
	var div = $("div#post");
	div.css("width", "550 px");
	let slika = $('<img src="' + fotografija.slika + '" id="slika"/></td>');
	slika.css("width", "90%");
	let dugmeBrisanje = $('<i id="brisanjeFotografije" class="bi bi-trash-fill"></i>');
	div.append(slika).append(dugmeBrisanje);
	reakcijaBrisanje(fotografija);
	upisiKomentare(fotografija);
}

function prikazFotografije(fotografija) {
	if (fotografija.opis) {
		prikazSaOpisom(fotografija);
	}else {
		prikazBezOpisa(fotografija);
	}	
}

$(document).ready(function() {

    const url = new URLSearchParams(window.location.search);
	
	if(url.has('idSopstvena')) {
		var id = url.get('idSopstvena');
		$.get({
	        url: "../rest/sopstveneSlike/slika?id=" + id,
	        headers: {
		        'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
		    },
			contentType:"application/json",
			dataType:"json",
	        success: function(odgovor) {
	            prikazFotografije(odgovor);
	        },
	        error: function(odgovor) {
	            alert(odgovor.responseText);
	        }
    	});	
	}

})
