$(window).bind("pageshow", function(event){
	if (event.originalEvent.persisted){
		location.reload();	
	}
});

function proveraUnosa(unos, poruka, polje) {
	let tdGreska = $(polje);
	if (unos === ""){
		tdGreska.text(poruka);
        tdGreska.removeAttr("hidden");
        return false;
	}
	tdGreska.attr("hidden", true);
    return true;
}


$(document).ready(function() {

    $("#forma").submit(function(event) {
        event.preventDefault();

        let korisnickoIme = $('input[name="korisnickoIme"]').val();
        let lozinka = $('input[name="lozinka"]').val();

		if (proveraUnosa(korisnickoIme, "Niste uneli korisnicko ime!", "#greskaKorIme") &&
			proveraUnosa(lozinka, "Niste uneli lozinku!", "#greskaLozinka")) {
			
			$.post({
	            url: "../rest/prijava?korisnickoIme=" + korisnickoIme + "&lozinka=" + lozinka,
	            contentType: "application/json",
	            success: function(odgovor) {
					sessionStorage.setItem("jwt", odgovor.jwt);
					if (odgovor.uloga === "ADMINISTRATOR"){
						window.location.assign("http://localhost:8088/wp-projekat/html/pocetna_admin.html");
					}else{
						window.location.assign("http://localhost:8088/wp-projekat/html/sopstveni_profil.html");
					}  
	            },
	            error: function(odgovor) {
	                if (odgovor.responseText.includes("lozinku")) {
						$("#greskaLozinka").text(odgovor.responseText);
						$("#greskaLozinka").removeAttr("hidden");
					}else {
						$("#greskaKorIme").text(odgovor.responseText);
						$("#greskaKorIme").removeAttr("hidden");
					}
	            }
        	});			
			
		}

    });

});