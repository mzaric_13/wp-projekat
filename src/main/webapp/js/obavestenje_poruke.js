function dobaviPosiljaocaPrikazObavestenja(deloviPoruke) {
	$.get({
            url: "../rest/necijiProfil/dobaviKorisnika?korisnickoIme=" + deloviPoruke[0],
            headers:{
                'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
            },
            contentType: "application/json",
            dataType: "json",
            success: function(korisnik) {
                alert("Imate poruku od: " + korisnik.ime + " " + korisnik.prezime + "!");
            }
    });
}

var socket;
$(document).ready(function() {

    let host = "ws://localhost:8088/wp-projekat/websocket";
	socket = new WebSocket(host);

    socket.onmessage = function(poruka) {
        let deloviPoruke = poruka.data.split('-');

		$.get({
	        url: "../rest/prijava/provera",
	        headers:{
	            'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
	        },
	        contentType: "application/json",
	        dataType: "json",
	        success: function(odgovor) {
	            if (deloviPoruke[1] === odgovor.korisnickoIme) {
					dobaviPosiljaocaPrikazObavestenja(deloviPoruke);
				}
	        },
    	});
    }

    socket.onclose = function() {
		socket = null;
	}

});