function svaPoljaPrazna(ime, prezime, pocetniDatum, krajnjiDatum){
    if (ime === "" && prezime === "" && pocetniDatum === "" && krajnjiDatum === ""){
        alert("Niste uneli nijedan kriterijum pretraživanja!");
        return true;
    }
    return false;
}

function obaDatumaUneta(pocetniDatum, krajnjiDatum) {
    if ((pocetniDatum === "" && krajnjiDatum != "") || (pocetniDatum != "" && krajnjiDatum === "")){
        alert("Oba datuma moraju biti uneta ako se pretražuje po tom kriterijumu!");
        return false;
    }    
    return true;
}

$(window).bind("pageshow", function(event){
	if (event.originalEvent.persisted){
		location.reload();	
	}
});


$(document).ready(function(){
	
	$("#pretraga_korisnika").submit(function(event){
        let ime = $('input[name="ime"]').val();
        let prezime = $('input[name="prezime"]').val();
        let pocetniDatum = $('input#pocetniDatum').val();
        let krajnjiDatum = $('input#krajnjiDatum').val();
		let url;
		
		if(sessionStorage.getItem("jwt")) {
			url = "../rest/pretragaKorisnika/ulogovani?ime=" + ime +
			 "&prezime=" + prezime + "&pocetniDatum=" + pocetniDatum + "&krajnjiDatum=" + krajnjiDatum;
		}else{
			url = "rest/pretragaKorisnika?ime=" + ime + 
			"&prezime=" + prezime + "&pocetniDatum=" + pocetniDatum + "&krajnjiDatum=" + krajnjiDatum;
		}
		
        if (!svaPoljaPrazna(ime, prezime, pocetniDatum, krajnjiDatum) 
			&& obaDatumaUneta(pocetniDatum, krajnjiDatum)){
			if (sessionStorage.getItem("jwt")){
				$.get({
					url: url,
					headers:{'Authorization':'Bearer ' + sessionStorage.getItem('jwt')},
					contentType:"application/json",
					dataType:"json", 
					success:function(korisnici){
						sessionStorage.setItem("pretrazeniKorisnici", JSON.stringify(korisnici));
						location.assign("http://localhost:8088/wp-projekat/html/pretrazeni_korisnici.html");
					},
					error:function(){
						alert("Greška!");
					}
				});
			}else{
				$.get({
					url: url,
					success:function(korisnici){
						sessionStorage.setItem("pretrazeniKorisnici", JSON.stringify(korisnici));
						location.assign("http://localhost:8088/wp-projekat/html/pretrazeni_korisnici.html");
					},
					error:function(){
						alert("Greška!");
					}
				});
       		}
		event.preventDefault();
		}
	});
})