function proveraImePrezime(unos) {
    var regex = new RegExp('([A-Z]{1})([a-z]+)([^0-9]*)$');
    if (regex.test(unos)) {
        return true;
    }
    return false;
}

function proveraEmail(email) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
        return true;
    }
    return false;
}

function proveraLozinka(lozinka, potvrdaLozinke) {
    if (lozinka === potvrdaLozinke) {
        return true;
    }
    return false;
}

function validacijaEmail(email) {
    var tdGreska = $("#greskaEmail");
    if (email === "" || !proveraEmail(email)) {
        tdGreska.text("Niste uneli email ili je format neispravan!");
        tdGreska.removeAttr("hidden");
        return false;
    }
    tdGreska.attr("hidden", true);
    return true;
}

function validacijaImePrezime(unos, polje, provera) {
    var tdGreska = $(polje);
    if (unos === "" || !proveraImePrezime(unos)) {
        tdGreska.text("Niste uneli " +  provera + " ili je format neispravan!");
        tdGreska.removeAttr("hidden");
        return false;
    }
    tdGreska.attr("hidden", true);
    return true;
}

function validacijaPol(pol) {
    var tdGreska = $("#greskaPol");
    if (pol === "" || pol === "Pol") {
        tdGreska.text("Niste odabrali pol!");
        tdGreska.removeAttr("hidden");
        return false;
    }
    tdGreska.attr("hidden", true);
    return true;
}

function validacijaDatumRodjenja(datumRodjenja) {
    var tdGreska = $("#greskaDatum");
    if (!datumRodjenja) {
        tdGreska.text("Niste uneli datum rodjenja!");
        tdGreska.removeAttr("hidden");
        return false;
    }
    tdGreska.attr("hidden", true);
    return true;
}

function validacijaNeunetoPolje(vrednost, polje, poruka) {
    var tdGreska = $(polje);
    if (vrednost === "") {
        tdGreska.text(poruka);
        tdGreska.removeAttr("hidden");
        return false;
    }
    tdGreska.attr("hidden", true);
    return true;
}

function validacijaUnosa(korisnickoIme, email, ime, prezime, pol, datumRodjenja, lozinka, potvrdaLozinke) {

    if (!validacijaNeunetoPolje(korisnickoIme, "#greskaKorIme", "Niste uneli korisničko ime!") || !validacijaEmail(email) ||
        !validacijaImePrezime(ime, "#greskaIme", "ime") || !validacijaImePrezime(prezime, "#greskaPrezime", "prezime") ||
        !validacijaPol(pol) || !validacijaDatumRodjenja(datumRodjenja) || !validacijaNeunetoPolje(lozinka, "#greskaLozinka", "Niste uneli lozinku!") ||
        !validacijaNeunetoPolje(potvrdaLozinke, "#greskaPotvrdaLozinka", "Niste potvrdili lozinku!")) {
            return false;
    } else {
		var tdGreska = $("#greskaPotvrdaLozinka");
        if (!proveraLozinka(lozinka, potvrdaLozinke)) {
            tdGreska.text("Uneta potvrda lozinke se ne poklapa sa zadatom lozinkom!");
            tdGreska.removeAttr("hidden");
            return false;
        }else{
            tdGreska.attr("hidden", true);
            return true;
        }
    }
}

$(window).ready(function() {
    $("#forma").submit(function(event) {
        event.preventDefault();

        let korisnickoIme = $('input[name="korisnickoIme"]').val();
        let email = $('input[name="email"]').val();
        let ime = $('input[name="ime"]').val();
        let prezime = $('input[name="prezime"]').val();
        let pol = $('select[name="pol"]').find(":selected").val();
        let datumRodjenja = $('input#datum').val();
        let lozinka = $('input[name="lozinka"]').val();
        let potvrdaLozinke = $('input[name="potvrdaLozinke"]').val();

        if (validacijaUnosa(korisnickoIme, email, ime, prezime, pol, datumRodjenja, lozinka, potvrdaLozinke)) {
            
            $.post({
                url: "../rest/registracija",
                contentType: "application/json",
                data: JSON.stringify({
                    korisnickoIme: korisnickoIme,
                    lozinka: lozinka,
                    email: email,
                    ime: ime,
                    prezime: prezime,
                    datumRodjenja: datumRodjenja,
                    pol: pol,
                    privatanNalog: false,
                    aktivan: true
                }),
                success: function(odgovor) {
					alert("Korisnik " + odgovor.korisnickoIme + " je registrovan!")
					
					let urlLogin = "../rest/prijava?korisnickoIme=" + odgovor.korisnickoIme + "&lozinka=" + odgovor.lozinka
					
					$.post({
           				url: urlLogin,
            			contentType: "application/json",
            			success: function(odgovor) {
                			sessionStorage.setItem("jwt", odgovor.jwt);
                			window.location.assign("http://localhost:8088/wp-projekat/html/sopstveni_profil.html");
            			},
            			error: function(odgovor) {
               	 			alert(odgovor.responseText);
            			}
        			});

					
                },
                error: function(odgovor) {
                    if (odgovor.status == 400) {
						alert("Korisnik sa zadatim korisnickim imenom vec postoji!");
					}else {
						alert("Greska pri registraciji!");
					}
                }
            });
        }
    });
})