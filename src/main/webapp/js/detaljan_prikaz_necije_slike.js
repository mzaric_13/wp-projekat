function prikaziKomentar(komentar, korisnik) {
    var tabela = $("#komentari");
	let tr = $("<tr></tr>");
    let tdSlika = $('<td><img src="' + komentar.korisnik.profilnaSlika + '" alt="" class="rounded-circle fiksiraj_profilne" /></td>')
	let tdKorisnik = $('<td>' + komentar.korisnik.ime + ' ' + komentar.korisnik.prezime + '</td>');
	let tdOpis = $('<td>' + komentar.tekst + '</td>');
	let tdBrisanje = $('<td></td>');
	if (korisnik.korisnickoIme === komentar.korisnik.korisnickoIme) {
		let dugmeBrisanje = $('<i id="brisanjeKomentara" class="bi bi-trash-fill"></i>');
		$(dugmeBrisanje).click(function(event) {
			event.preventDefault();
			$.ajax({
				type: "PUT",
				url: "../rest/drugeSlike/brisanjeKomentar",
				headers: {
		        	'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
		    	},
				contentType:"application/json",
				dataType:"json",
				data: JSON.stringify(komentar),
				success: function(odgovor){
					alert("Komentar: " + odgovor.tekst + ", je obrisan!");
					window.location.reload();
				},
				error: function(odgovor) {
					alert(odgovor.responseText);
				}
			});
		});
		tdBrisanje.append(dugmeBrisanje);
	}
	tr.append(tdSlika).append(tdKorisnik).append(tdOpis).append(tdBrisanje);
	tabela.append(tr);
}

function prikaziPoljeKomentar(idFotografije, korisnik) {
    var tabela = $("#komentari");
    let tr = $('<tr></tr>');
    let tdKomentar = $('<td class="unos" colspan="2"></td>');
    let div = $('<div class="form-group"></div>');
	let input;
	if (!sessionStorage.getItem("admin")){
		input = $('<input class="unos" type="text" class="form-control" name="komentar" placeholder="Vaš komentar" />');
	} else {
		input = $ ('<input class="unos" type="text" class="form-control" name="razlogBrisanja" placeholder="Razlog brisanja" />')
	}
    div.append(input);
    tdKomentar.append(div);
    let tdDugme = $('<td></td>');
	let dugme;
	if (!sessionStorage.getItem("admin")){
		dugme = $('<button type="button" class="btn btn-success" id="potvrdaKomentar">Komentariši</button>');
	} else {
		dugme = $('<button type="button" class="btn btn-danger" id="potvrdaBrisanja">Obriši</button>');	
	}
	if (!sessionStorage.getItem("admin")){
		dugme.click(function(event) {
			event.preventDefault();
			let komentar = $(input).val();
			if (komentar) {
				$("p#greska").text("");
				$.post({
					url: "../rest/drugeSlike/komentari?idSlike=" + idFotografije,
					headers: {
	            		'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
			        },
			        contentType:"application/json",
			        dataType:"json",
					data: JSON.stringify({
						tekst: komentar,
						datumKomentara: Date.now(),
						obrisan: false
					}),
					success: function(odgovor) {
						alert("Uspesno ste postavili komentar");
						window.location.reload();
					},
					error: function(odgovor) {
						alert(odgovor.responseText);
					}
				});
			}else{
				$("p#greska").text("Niste uneli tekst komentara!");
			}
		}); 
	} else {
		dugme.click(function(event){
			event.preventDefault();
			let razlogBrisanja = $(input).val();
			if (razlogBrisanja) {
				$("p#greska").text("");
				$.post({
					url: "../rest/drugeSlike/izbrisiSliku?idSlike=" + idFotografije,
					headers : {
						'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
					},
					contentType:"application/json",
			        dataType:"json",
					success: function(odgovor){
						posaljiRazlogBrisanja(korisnik, razlogBrisanja, odgovor);
						history.back();
					},
					error: function(odgovor) {
						alert(odgovor.responseText);
					}
				})
			} else {
				$("p#greska").text("Niste uneli razlog brisanja fotografije!");
			}
		})
	}  

	tdDugme.append(dugme);
    tr.append(tdKomentar).append(tdDugme);
    tabela.append(tr);
}

function posaljiRazlogBrisanja(korisnik, razlogBrisanja, korisnikFotografija) {
	let poruka = "";
	poruka += korisnik.korisnickoIme + "-";
	poruka += korisnikFotografija.korisnickoIme + "-";
	poruka += razlogBrisanja;
	socket.send(poruka);
	$.post({
		url:"../rest/poruke",
		headers:{
			   'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
		},
		contentType: "application/json",
		dataType: "json",
		data: JSON.stringify({
			posiljalac: korisnik,
			primalac: korisnikFotografija, 
			sadrzaj: razlogBrisanja,
			datumSlanja: Date.now()
		}),
		success: function(odgovor) {
			alert("Uspesno obrisana objava!");
		}
	});
}

function prikaziKomentare(fotografija, korisnik) {
    for (var kom of fotografija.komentari) {
        if (kom.obrisan) continue;
        prikaziKomentar(kom, korisnik);
    }
    prikaziPoljeKomentar(fotografija.id, korisnik);
}

function dobaviUlogovanogKorisnika(fotografija) {
	$.get({
		url: "../rest/prijava/provera",
		headers: {
            'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
        },
        contentType:"application/json",
        dataType:"json",
		success: function(odgovor) {
			prikaziKomentare(fotografija, odgovor)
		},
		error: function(odgovor) {
            alert(odgovor.responseText);
        }
	});
}

function prikazNecijeSaOpisom(fotografija) {
	var divPost = $("div#post");
	let slika = $('<img src="' + fotografija.slika + '" id="slika"/></td>');
	divPost.append(slika);
	let divOpis = $('<div id="opis"></div>');
    divOpis.text(fotografija.opis);
	divPost.append(divOpis);
	dobaviUlogovanogKorisnika(fotografija);
}

function prikazNecijeBezOpisa(fotografija) {
	var div = $("div#post");
	div.css("width", "500 px");
	let slika = $('<img src="' + fotografija.slika + '" id="slika"/></td>');
	slika.css("width", "100%");
	div.append(slika);
	dobaviUlogovanogKorisnika(fotografija);
}

function prikazNecijeFotografije(fotografija) {
	if (fotografija.opis) {
		prikazNecijeSaOpisom(fotografija);
	}else {
		prikazNecijeBezOpisa(fotografija);
	}
}

var socket;
$(document).ready(function() {
	
	var host = "ws://localhost:8088/wp-projekat/websocket";
	socket = new WebSocket(host);

    const url = new URLSearchParams(window.location.search);

	if(url.has('idNecija')) {
        var id = url.get('idNecija');
		$.get({
        	url: "../rest/drugeSlike/slika?id=" + id,
        	headers: {
	        	'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
	    	},
			contentType:"application/json",
			dataType:"json",
	        success: function(odgovor) {
	            prikazNecijeFotografije(odgovor);
	        },
	        error: function(odgovor) {
	            alert(odgovor.responseText);
	        }
    	});	
	}
	
	socket.onclose = function() {
		socket = null;
	}
})