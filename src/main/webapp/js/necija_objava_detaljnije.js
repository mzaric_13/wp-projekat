function prikaziKomentar(komentar, korisnik) {
    var tabela = $("#komentari");
	let tr = $("<tr></tr>");
    let tdSlika = $('<td><img src="' + komentar.korisnik.profilnaSlika + '" alt="" class="rounded-circle fiksiraj_profilne" /></td>')
	let tdKorisnik = $('<td>' + komentar.korisnik.ime + ' ' + komentar.korisnik.prezime + '</td>');
	let tdOpis = $('<td>' + komentar.tekst + '</td>');
	let tdBrisanje = $('<td></td>');
	if (korisnik.korisnickoIme === komentar.korisnik.korisnickoIme) {
		let dugmeBrisanje = $('<i id="brisanjeKomentara" class="bi bi-trash-fill"></i>');
		$(dugmeBrisanje).click(function(event) {
			event.preventDefault();
			$.ajax({
				type: "PUT",
				url: "../rest/drugeSlike/brisanjeKomentar",
				headers: {
		        	'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
		    	},
				contentType:"application/json",
				dataType:"json",
				data: JSON.stringify(komentar),
				success: function(odgovor){
					alert("Komentar: " + odgovor.tekst + ", je obrisan!");
					window.location.reload();
				},
				error: function(odgovor) {
					alert(odgovor.responseText);
				}
			});
		});
		tdBrisanje.append(dugmeBrisanje);
	}
	tr.append(tdSlika).append(tdKorisnik).append(tdOpis).append(tdBrisanje);
	tabela.append(tr);
}

function prikaziPoljeKomentar(idObjave, korisnik) {
    var tabela = $("#komentari");
    let tr = $('<tr></tr>');
    let tdKomentar = $('<td colspan="2"></td>');
    let div = $('<div class="form-group"></div>');
	let input;
	if (!sessionStorage.getItem("admin")){
		input = $('<input class="unos" type="text" class="form-control" name="komentar" placeholder="Vaš komentar" />');
	} else {
		input = $('<input class="unos" type="text" class="form-control" name="razlogBrisanja" placeholder="Razlog brisanja" />');
	}
    div.append(input);
    tdKomentar.append(div);
    let tdDugme = $('<td></td>');
	let dugme;
	if (!sessionStorage.getItem("admin")){
		dugme = $('<button type="button" class="btn btn-success" id="potvrdaKomentar">Komentariši</button>');
	} else {
		dugme = $('<button type="button" class="btn btn-danger" id="potvrdaBrisanja">Obriši</button>');
	}
	if (!sessionStorage.getItem("admin")){
		dugme.click(function(event) {
			event.preventDefault();
			let komentar = $(input).val();
			if (komentar) {
				$("p#greska").text("");
				$.post({
					url: "../rest/drugeObjave/komentari?idObjave=" + idObjave,
					headers: {
	            		'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
			        },
			        contentType:"application/json",
			        dataType:"json",
					data: JSON.stringify({
						tekst: komentar,
						datumKomentara: Date.now(),
						obrisan: false
					}),
					success: function(odgovor) {
						alert("Uspesno ste postavili komentar");
						window.location.reload();
					},
					error: function(odgovor) {
						alert(odgovor.responseText);
					}
				});
			}else{
				$("p#greska").text("Niste uneli tekst komentara!");
			}
		});    
	} else {
		dugme.click(function(event){
			event.preventDefault();
			let razlogBrisanja = $(input).val();
			if (razlogBrisanja) {
				$("p#greska").text("");
				$.post({
					url: "../rest/drugeObjave/izbrisiObjavu?idObjave=" + idObjave,
					headers : {
						'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
					},
					contentType:"application/json",
			        dataType:"json",
					success: function(odgovor){
						posaljiRazlogBrisanja(korisnik, razlogBrisanja, odgovor);
						history.back();
					},
					error: function(odgovor) {
						alert(odgovor.responseText);
					}
				})
			} else {
				$("p#greska").text("Niste uneli razlog brisanja objave!");
			}
		})
	}
	tdDugme.append(dugme);
    tr.append(tdKomentar).append(tdDugme);
    tabela.append(tr);
}

function posaljiRazlogBrisanja(korisnik, razlogBrisanja, korisnikObjava) {
	let poruka = "";
	poruka += korisnik.korisnickoIme + "-";
	poruka += korisnikObjava.korisnickoIme + "-";
	poruka += razlogBrisanja;
	socket.send(poruka);
	$.post({
		url:"../rest/poruke",
		headers:{
			   'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
		},
		contentType: "application/json",
		dataType: "json",
		data: JSON.stringify({
			posiljalac: korisnik,
			primalac: korisnikObjava, 
			sadrzaj: razlogBrisanja,
			datumSlanja: Date.now()
		}),
		success: function(odgovor) {
			alert("Uspesno obrisana objava!");
		}
	});
}

function prikazKomentara(komentari, objava, korisnik) {
    for(let kom of komentari) {
		if(kom.obrisan === true) continue;
		prikaziKomentar(kom, korisnik);
	}
	prikaziPoljeKomentar(objava.id, korisnik);
}

function dobaviUlogovanogKorisnika(komentari, objava) {
	$.get({
		url: "../rest/prijava/provera",
		headers: {
            'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
        },
        contentType:"application/json",
        dataType:"json",
		success: function(odgovor) {
			prikazKomentara(komentari, objava, odgovor)
		},
		error: function(odgovor) {
            alert(odgovor.responseText);
        }
	});
}

function dobaviKomentareObjave(objava) {
    $.get({
        url: "../rest/drugeObjave/komentari?id=" + objava.id,
        headers: {
            'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
        },
        contentType:"application/json",
        dataType:"json",
        success: function(odgovor) {
            dobaviUlogovanogKorisnika(odgovor, objava);
        },
        error: function(odgovor) {
            alert(odgovor.responseText);
        }
    });
}

function necijaObjavaSaSlikom(objava) {
	var divPost = $("div#post");
	let slika = $('<img src="' + objava.slika + '" id="slikaObjave"/></td>');
	divPost.append(slika);
    $("#tekst").text(objava.tekst);
	dobaviKomentareObjave(objava);
}

function necijaObjavaBezSlike(objava) {
	var divPost = $("div#post");
	divPost.css("width", "500 px");
	divPost.css("height", "200 px");
	var divTekst = $("div#tekst");
	divTekst.css("width", "100%");
	divTekst.text(objava.tekst);
	dobaviKomentareObjave(objava);
}

function prikazNecijeObjave(objava) {
	if (objava.slika) {
		necijaObjavaSaSlikom(objava);
	}else {
		necijaObjavaBezSlike(objava);
	}	
}

var socket;
$(document).ready(function() {
	
	var host = "ws://localhost:8088/wp-projekat/websocket";
	socket = new WebSocket(host);

    const url = new URLSearchParams(window.location.search);

	if(url.has('idNecija')) {
        var id = url.get('idNecija');
		$.get({
        	url: "../rest/drugeObjave/objava?id=" + id,
        	headers: {
	        	'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
	    	},
			contentType:"application/json",
			dataType:"json",
	        success: function(odgovor) {
	            prikazNecijeObjave(odgovor);
	        },
	        error: function(odgovor) {
	            alert(odgovor.responseText);
	        }
    	});	
	}
	
	socket.onclose = function() {
		socket = null;
	}
})