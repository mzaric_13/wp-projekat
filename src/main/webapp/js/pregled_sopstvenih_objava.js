function prikazObjava(korisnik, objave) {
    var divObjave = $("div#objave");
	for (let objava of objave) {
		if (objava.obrisan) continue;
		let div = $('<div id="' + objava.id + '" class="objava"></div>');
		let divProfil = $('<div id="profil"></div>');
		let profilna = $('<img class="profilnaSlika" src="' + korisnik.profilnaSlika + '" />');
		divProfil.text(korisnik.ime + ' ' + korisnik.prezime);
		divProfil.append(profilna);
		divProfil.css("color", "blue");
		let opis = $('<div id="opis">' + objava.tekst + '</div>');
		div.append(divProfil).append(opis);
		if (objava.slika) {
			let	sl = $('<img id="slika" class="fiksiraj_objave" src="' + objava.slika + '" />');
			div.append(sl);
		}
		div.click(function(event) {
			event.preventDefault();
			
			window.location.assign("http://localhost:8088/wp-projekat/html/detaljan_prikaz_objave.html?idSopstvena=" + objava.id);
		});
		divObjave.append(div);
	}
}

$(document).ready(function() {
	
	$.get({
		url: "../rest/prijava/provera",
		headers: {
            'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
        },
        contentType:"application/json",
        dataType:"json",
		success: function(odgovor) {
			$.get({
			    	url: "../rest/sopstveneObjave",
			        headers: {
				        'Authorization':'Bearer ' + sessionStorage.getItem('jwt')
				    },
					contentType:"application/json",
					dataType:"json",
			        success: function(objave) {
			            prikazObjava(odgovor, objave);
			        },
			        error: function(odgovor) {
			            alert(odgovor.responseText);
			        }
    		});
		},
		
	});
})