/*let hrefSlika = $('<a href="http://localhost:8088/wp-projekat/html/neciji_profil.html?korisnickoIme='
			 + zajednickiPrijatelji[nextItem + j].korisnickoIme + '"><img src="'
			 + zajednickiPrijatelji[nextItem + j].profilnaSlika + 
			 '" alt="" class="rounded-circle fiksiraj_velicinu_prijatelja"></a>');*/

function iscrtajRed(korisnik){
	let red = $('<tr> </tr>');
	let tdProfilna = $('<td> </td>');
	let hrefSlika = $('<a href="../html/neciji_profil.html?korisnickoIme=' + korisnik.korisnickoIme + '">'
					+ '<img src="' + korisnik.profilnaSlika + 
					'" alt="" class="rounded-circle fiksiraj_profilne"></a>');
	tdProfilna.append(hrefSlika);
	let tdImePrezime = $('<td>' + korisnik.ime + " " + korisnik.prezime + '</td>');
	let tdDatumRodjenja = "";
	let tdBlokiranje = "";
	if (sessionStorage.getItem("admin")){
		if (korisnik.aktivan){
			tdBlokiranje = $('<td><button type="button" class="btn btn-danger" id="' + korisnik.korisnickoIme +
		 	'"><i class="fas fa-ban"></i></td>');
		}
		else if (!korisnik.aktivan){
			tdBlokiranje = $('<td><button type="button" class="btn btn-success" id="' + korisnik.korisnickoIme +
		 	'"><i class="fas fa-unlock"></i></td>');
		}
	}else{
		let datum = new Date(korisnik.datumRodjenja);
		tdDatumRodjenja = $('<td> Datum rođenja: ' + datum.toLocaleDateString() + '</td>');
	}
	red.append(tdProfilna)
		.append(tdImePrezime);
	if (sessionStorage.getItem("admin")){
		red.append(tdBlokiranje);
	}else{
		red.append(tdDatumRodjenja);
	}
	$("table").append(red);	
}


function kreirajTabelu(korisnici){
	for (let korisnik of korisnici){
		iscrtajRed(korisnik);
	}
	if (sessionStorage.getItem("admin")){
		let buttons = $("table tr td button");
		for (let b of buttons){
			$(b).bind("click", function(event){
			let id = $(this).attr("id");
			if ( id != "potvrdaUnosa"){
				$.post({
					url : "../rest/admin/promeniStatus?id=" + id,
					headers: {'Authorization':'Bearer ' + sessionStorage.getItem('jwt')},
					data: JSON.stringify(korisnici),
					contentType: "application/json",
					dataType: "json",
					success: function(korisnici){
						sessionStorage.setItem("pretrazeniKorisnici", JSON.stringify(korisnici));
						location.reload();
					},
					error: function(){
						alert("Greška!");
					}
				});	
			}
		});
		}			
	}
}

$(document).ready(function(){
	let korisnici = JSON.parse(sessionStorage.getItem("pretrazeniKorisnici"));
	kreirajTabelu(korisnici);
	if (!sessionStorage.getItem('jwt')){
		$("#potvrdaUnosa").click(function(){
			let opcija = $('select[name="opcija"]').find(":selected").val();
			$.post({
				url: "../rest/pretragaKorisnika?opcija=" + opcija,
				contentType: "application/json",
				dataType: "json",
				data: JSON.stringify(korisnici),
				success: function(sortiraniKorisnici){
					sessionStorage.setItem("pretrazeniKorisnici", JSON.stringify(sortiraniKorisnici));
					location.reload();
				},
				error: function(){
					alert("Greška.");
				}
			});
		});
	}else{
		$("#potvrdaUnosa").click(function(){
			let opcija = $('select[name="opcija"]').find(":selected").val();
			$.post({
				url: "../rest/pretragaKorisnika/sortUlogovani?opcija=" + opcija,
				headers: {'Authorization':'Bearer ' + sessionStorage.getItem('jwt')},
				contentType: "application/json",
				dataType: "json",
				data: JSON.stringify(korisnici),
				success: function(sortiraniKorisnici){
					sessionStorage.setItem("pretrazeniKorisnici", JSON.stringify(sortiraniKorisnici));
					location.reload();
				},
				error: function(){
					alert("Greška.");
				}
			});
		});	
	}
})